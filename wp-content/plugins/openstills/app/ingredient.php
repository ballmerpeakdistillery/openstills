<?php
/**
 * All ingredient related functions should go here
 */


/**
 * Creates the ingredient post type
 */
function createIngredientPostType() {

    register_post_type('ingredient',
        array(
            'labels' => array(
                'name'                  => __('Ingredients'),
                'singular_name'         => __('Ingredient')
            ),
            'public'        => true,
            'has_archive'   => true,
            'show_in_rest'  => true,
            'rewrite'       => array('slug' => 'ingredeint'),
            'supports'      => array('title', 'editor', 'custom-fields', 'excerpt', 'thumbnail'),
        )
    );
    flush_rewrite_rules();
}
add_action('init', 'createIngredientPostType');

function createIngredientTaxonomies() {
    register_taxonomy(
        'ingredient-category',
        array('ingredient'),
        array(
            'label'                 => __('Ingredient Category'),
            'rewrite'               => array('slug' => 'ingredient-category'),
            'hierarchical'          => true,
            'show_in_rest'          => true,
            'rest_base'             => 'category',
            'rest_controller_class' => 'WP_REST_Terms_Controller'
        )
    );
}
add_action('init', 'createIngredientTaxonomies');


/**
 * Registering post meta for use in the api
 * this is too much to do every time you add a new field
 * need to automate this
 */
function ingredientPostMeta() {
    register_meta('post', 'units', [
        'show_in_rest'   => true,
        'object_subtype' => 'ingredient',
    ]);
    register_meta('post', 'inventory', [
        'show_in_rest'   => true,
        'object_subtype' => 'ingredient',
    ]);
    register_meta('post', 'price-per-unit', [
        'show_in_rest'   => true,
        'object_subtype' => 'ingredient',
    ]);
    register_meta('post', 'supplier', [
        'show_in_rest'   => true,
        'object_subtype' => 'ingredient',
    ]);
}
add_action("init", "ingredientPostMeta");


/**
 * Ingredient class so we can configure objects
 */
class ingredient extends osBase {

    public function __construct() {
        // do the basics
        parent::__construct();

        $this->addConfig(
            "Inventory",
            "Current Inventory",
            "text",
            array(
                "wrapperClasses"    => "field-3",
                "wp_field"          => "meta_inventory"
            )
        );
        $this->addConfig(
            "Inventory",
            "Units",
            "select",
            array(
                "wrapperClasses"    => "field-3",
                "options"           => array(
                    "units"             => "Units",
                    "ml"                => "Milliliters",
                    "gal"               => "Gallons",
                    "g"                 => "Grams",
                    "oz"                => "Ounces",
                    "lb"                => "Pounds"
                ),
                "wp_field"          => "meta_units"
            )
        );
        $this->addConfig(
            "Inventory",
            "Price Per Unit",
            "text",
            array(
                "wrapperClasses"    => "field-3",
                "wp_field"          => "meta_price"
            )
        );
        $this->addConfig(
            "Inventory",
            "Supplier",
            "post",
            array(
                "post_type"         => "ingredient",
                "wp_field"          => "meta_supplier"
            )
        );
    }

}