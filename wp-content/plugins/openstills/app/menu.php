<?php
/********************************************************************
* Menu related functions
* ******************************************************************/

/**
 * Take the JSON that gets submitted and save
 */
function saveMenu() {
    // this is json
    $menu = $_POST["menu"];
    // save to db
    update_option("osMenu", $menu);
}

/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'savemenu', array(
        'methods' => 'POST',
        'callback' => 'saveMenu',
    ) );
});


function openstills_nav() {
    $menu = get_option("osMenu");

    echo '<ul id="user-menu">';
    foreach ($menu as $menuItem) {
        switch ($menuItem["type"]) {
            case "header":
                echo '<li class="menu-header menu-item is-uppercase has-text-weight-bold">' . $menuItem["value"] . '</li>';
                break;
            case "object":
                echo '<li class="menu-item object-menu-item has-children"><a>' . $menuItem["value"] . '<i class="fa fa-chevron-down is-pulled-right"></i></a>';
                echo '<ul class="sub-menu">';
                echo '<li><a>View All <i class="fa fa-eye is-pulled-right is-size-6"></i></a></li>';
                echo '<li><a>Add <i class="fa fa-plus-square is-pulled-right is-size-6"></i></a></li>';
                echo '<li><a>Settings <i class="fa fa-gears is-pulled-right is-size-6"></i></a></li>';
                echo '</ul>';
                echo '</li>';
                break;
        }
    }
    echo '</ul>';
}


/**
 * Create a nav menu with very basic markup.
 *
 * @author Thomas Scholz http://toscho.de
 * @version 1.0
 */
class T5_Nav_Menu_Walker_Simple extends Walker_Nav_Menu
{
    /**
     * Start the element output.
     *
     * @param  string $output Passed by reference. Used to append additional content.
     * @param  object $item   Menu item data object.
     * @param  int $depth     Depth of menu item. May be used for padding.
     * @param  array $args    Additional strings.
     * @return void
     */
    public function start_el( &$output, $item, $depth, $args )
    {
        $classes = implode(" ", $item->classes);
        $output     .= '<li class="menu-item ' . $classes . '">';
        $attributes  = '';
        ! empty ( $item->attr_title )
            // Avoid redundant titles
            and $item->attr_title !== $item->title
            and $attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';
        ! empty ( $item->url )
            and $attributes .= ' href="' . esc_attr( $item->url ) .'"';
        $attributes  = trim( $attributes );
        $title       = apply_filters( 'the_title', $item->title, $item->ID );
        $item_output = "$args->before<a $attributes><i class='fa fa-$classes'></i>$args->link_before$title</a>"
                        . "$args->link_after$args->after";
        // Since $output is called by reference we don't need to return anything.
        $output .= apply_filters(
            'walker_nav_menu_start_el'
            ,   $item_output
            ,   $item
            ,   $depth
            ,   $args
        );
    }
    /**
     * @see Walker::start_lvl()
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @return void
     */
    public function start_lvl( &$output )
    {
        $output .= '<ul class="sub-menu">';
    }
    /**
     * @see Walker::end_lvl()
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @return void
     */
    public function end_lvl( &$output )
    {
        $output .= '</ul>';
    }
    /**
     * @see Walker::end_el()
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @return void
     */
    function end_el( &$output )
    {
        $output .= '</li>';
    }
}