<?php
/********************************************************************
* Object functionality
* ******************************************************************/

/********************************************************************
* Object information functions
* ******************************************************************/

/**
 * Get a list of object post types by file
 * @return array list of object types
 */
function getDefinedObjects() {
    $objects = array();
    // auto load all config files
    $files = glob(plugin_dir_path(__FILE__) . "../objects/*.json");
    foreach ($files as $file) {
        // get post type;
        $fileParts = explode("/", $file);
        $postType = str_replace("-config.json", "", end($fileParts));

        $objects[] = $postType;
    }

    return $objects;
}

/********************************************************************
* Create all available objects
* ******************************************************************/

/**
 * Take the JSON that gets submitted and save
 */
function saveObject() {
    // all of the $_POST stuff should be formatted exactly how we already want them
    // more or less we want to take that and output json to a file
    // for the record, PHP evaluating booleans as strings is bullshit
    // have to do all this extra strip slashes crap
    $data = json_decode(stripslashes($_POST["json"]), true);

    // create the file
    $postType = strtolower(str_replace(" ", "-", $data["config"]["labels"]["singular_name"]));
    $fileName = plugin_dir_path(__FILE__) . "../objects/" . $postType . "-config.json";
    try {
        $fileHandle = fopen($fileName, 'w');
        fwrite($fileHandle, json_encode($data));
    } catch (exception $e) {
        // error out something
    }
}

/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'saveobject', array(
        'methods' => 'POST',
        'callback' => 'saveObject',
    ) );
});

/**
 * Create all post types for saved config data
 * @return [type] [description]
 */
function createObjectPostTypes() {

    // auto load all config files
    $files = glob(plugin_dir_path(__FILE__) . "../objects/*.json");
    foreach ($files as $file) {
        // read in the data
        $json = json_decode(file_get_contents($file), true);

        $config = $json["config"];

        // add default supports
        $parsedConfig["supports"] = array('title', 'editor', 'custom-fields', 'excerpt', 'thumbnail');

        // get post type;
        $fileParts = explode("/", $file);
        $postType = str_replace("-config.json", "", end($fileParts));

        // register post type
        $results = register_post_type($postType, $config);

    }
    flush_rewrite_rules();
}
add_action('init', 'createObjectPostTypes');

function createObjectTaxonomies() {
    $files = glob(plugin_dir_path(__FILE__) . "../objects/*.json");
    foreach ($files as $file):
        $json = json_decode(file_get_contents($file), true);
        // get post type;
        $fileParts = explode("/", $file);
        $postType = str_replace("-config.json", "", end($fileParts));

        if (!array_key_exists("taxonomy", $json)) {
            continue;
        }
        $taxonomies = $json["taxonomy"];

        foreach ($taxonomies as $taxonomy):
            register_taxonomy(
                $postType . "-" . $taxonomy["slug"],
                array($postType),
                array(
                    'label'                 => __($taxonomy["label"]),
                    'rewrite'               => array('slug' => $taxonomy["slug"]),
                    'hierarchical'          => true,
                    'show_in_rest'          => true,
                    'rest_base'             => 'category',
                    'rest_controller_class' => 'WP_REST_Terms_Controller'
                )
            );
        endforeach;
    endforeach;
}
add_action('init', 'createObjectTaxonomies');


/********************************************************************
* Add query vars for creating and editing objects
* ******************************************************************/

function addObjectQueryVars($addedVars) {
    $addedVars[] = "object";
    return $addedVars;
}
add_filter('query_vars', 'addObjectQueryVars');

function addObjectRewriteRule($addedRules) {
    $addedNewRules = array(
        'setup/object/add/?$' => 'index.php?pagename=add-edit-object',
        'setup/object/edit/([^/]+)/?$' => 'index.php?pagename=add-edit-object&object=$matches[1]'
    );
    $addedRules = $addedNewRules + $addedRules;
    return $addedRules;
}
add_filter('rewrite_rules_array', 'addObjectRewriteRule');