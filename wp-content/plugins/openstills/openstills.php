<?php
/**
 * Plugin Name: OpenStills
 * Plugin URI: https://openstills.com
 * Description: Distillery management tool
 * Version: 1.0
 * Author: Your Name
 * Author URI: https://ballmerpeakdistillery.com
 */

/********************************************************************
* Base functionality here
* everything else should be in post type specific files
* ******************************************************************/


/**
 * Making API save requests
 * There's not a lot of security going on here
 * but ideally these are used in a more or less closed environment
 * this isn't a site you're showing to the public
 */

function localizeNonceData() {
    wp_localize_script('jquery', 'nonceData', array(
        'root_url' => get_site_url(),
        'nonce' => wp_create_nonce('wp_rest') //secret value created every time you log in and can be used for authentication to alter content 
    ));
}
add_action("wp_enqueue_scripts", "localizeNonceData");


/********************************************************************
* Post types and functionality
********************************************************************/

include("app/object.php");
include("app/menu.php");