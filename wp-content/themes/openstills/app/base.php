<?php
/********************************************************************
* Base functionality
* Stuff that doesn't really have a place anywhere else
* Helpers, etc
********************************************************************/


// auto p is the devil
remove_filter('the_content', 'wpautop');


/**
 * Locating templates so we can pass data to them
 * this is mostly used for input rendering
 */

function getTemplatePath($slug, $name = false) {

    do_action( "get_template_part_{$slug}", $slug, $name );

    $templates = array();
    if ( isset($name) )
        $templates[] = "{$slug}-{$name}.php";

    $templates[] = "{$slug}.php";

    $path = locate_template($templates);
    return $path;
}

function useTemplate($args) {
    $template = $args["template"];
    get_template_part($template);
}
add_shortcode("get_template", "useTemplate");



/********************************************************************
* WP API Base functionality
********************************************************************/

/**
 * Making API save requests
 * There's not a lot of security going on here
 * but ideally these are used in a more or less closed environment
 * this isn't a site you're showing to the public
 */

function localizeNonceData() {
    wp_localize_script('jquery', 'nonceData', array(
        'root_url' => get_site_url(),
        'nonce' => wp_create_nonce('wp_rest') //secret value created every time you log in and can be used for authentication to alter content 
    ));
}
add_action("wp_enqueue_scripts", "localizeNonceData");

/**
 * Query vars for adding new posts
 * this will take the value after /new/ as a query var
 */

function addPostQueryVars($addedVars) {
    $addedVars[] = "namespace";
    $addedVars[] = "postType";
    $addedVars[] = "postId";
    return $addedVars;
}
add_filter('query_vars', 'addPostQueryVars');

function addPostRewriteRule($addedRules) {
    $addedNewRules = array(
        'add/([^/]+)/([^/]+)/?$' => 'index.php?pagename=add&namespace=$matches[1]&postType=$matches[2]',
        'edit/([^/]+)/([^/]+)/([^/]*)/?$' => 'index.php?pagename=edit&namespace=$matches[1]&postType=$matches[2]&postId=$matches[3]'
    );
    $addedRules = $addedNewRules + $addedRules;
    return $addedRules;
}
add_filter('rewrite_rules_array', 'addPostRewriteRule');



/********************************************************************
* Helpers
********************************************************************/

function createId($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
