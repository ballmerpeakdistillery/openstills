<?php
/********************************************************************
* Comments functions
* Notes: We are making our own comments here because it's easier
* to customize what we want in this rather than to use wordpress'
* default comments.
* Might seem like a little bit of overkill but whatever
* you're not the one who wrote it to begin with
* *****************************************************************/


/*
* Create post types
*/
function createCommentPostType() {

    register_post_type('comment',
        array(
            'labels' => array(
                'name'                  => __('Comment'),
                'singular_name'         => __('Comment')
            ),
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => array('slug' => 'comment'),
            'supports'      => array('title', 'editor', 'custom-fields', 'excerpt', 'thumbnail', 'author'),
            'show_in_rest'  => true
        )
    );
    flush_rewrite_rules();
}

add_action('init', 'createCommentPostType');


/**
 * Sets the post ID for what we're commenting on
 * The post parent attribute isn't exposed to the api
 * so we have to do this unfortunately
 */
function setCommentParent($post, $request, $creating) {
    wp_update_post(
        array(
            'ID' => $post->ID,
            'post_parent' => $_POST["parent"]
        )
    );
}
add_action( 'rest_after_insert_comment', 'setCommentParent', 10, 3 );

/**
 * Outputs the comment form as well as any comments existing for the psot
 * @param  int $parentId post id that we're getting the comments for
 * @return html           total output of comments for a post
 */
function osComments($parentId) {

    echo '<div id="comments">';
    echo '<span class="subtitle has-text-weight-bold is-size-6">Comments:</span>';
    echo '<a class="button is-primary is-small is-pulled-right" data-action="unhideCommentForm">';
    echo '<span>Add Comment</span>';
    echo '<span class="icon"><i class="fa fa-plus"></i></span>';
    echo '</a>';

    // get comment form first
    $templateList = array(
        "templates/comments/form.php"
    );
    $template = locate_template($templateList, false, false);
    include($template);
    // get comment list
    $args = array(
        "post_type"         => "comment",
        "posts_per_page"    => -1,
        "post_parent"       => $parentId
    );
    $comments = get_posts($args);

    echo '<ul id="comment-list">';
    foreach ($comments as $comment) {
        $templateList = array(
            "templates/comments/comment.php"
        );
        $template = locate_template($templateList, false, false);
        include($template);
    }
    echo '</ul>';
    echo '</div>';
}

/**
 * add a notification for every user that is either on the comment thread or the post author
 */
function addCommentNotification($post, $request, $creating) {

    $users = array();

    // get all posts involved
    $parentPost = get_post($_POST["parent"]);
    $comments = get_posts(array(
        "post_type"     => "comment",
        "post_parent"   => $_POST["parent"]
    ));

    // push users in to array
    $users[] = $parentPost->post_author;
    foreach ($comments as $comment) {
        $users[] = $comment->post_author;
    }

    // get title and content
    $title = 'New comment on <a href="' . get_the_permalink($_POST["parent"]) . '">' . get_the_title($_POST["parent"]) . '</a>';
    $commentContent = $post->post_content;

    // only show the first 25 words of the content
    $explodedContent = explode(" ", $commentContent);
    if (count($explodedContent) > 25) {
        $commentContent = implode(" ", array_slice($explodedContent, 0, 25)) . "...";
    }

    // add notifications for cleaned up array
    addNotification("none", $title, $commentContent, array_unique($users));

}
add_action( 'rest_after_insert_comment', 'addCommentNotification', 10, 3 );