<?php
/********************************************************************
* Dashboard
********************************************************************/

function getDashboardItems() {

    // get user's dashboard info
    $currentUser = wp_get_current_user();
    $dashboardItems = json_decode(get_option("osDashboard_" . $currentUser->user_login));

    // get post info and template for each item
    foreach ($dashboardItems as $dashboardItem):
        // get relative template
        $template = get_post_meta($dashboardItem, "path", true);
        $itemId = $dashboardItem;
        include(get_template_directory() . "/templates/" . $template);
    endforeach;

}

function getItemHtml() {

    // get defiend template
    $template = get_post_meta($_POST["item"], "path", true);

    // put id in to variable
    $itemId = $_POST["item"];

    // get that template content
    ob_start();
    include(get_template_directory() . "/templates/" . $template);
    $html = ob_get_clean();

    return array(
        "html" => $html
    );
}
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'dashboard/getitemhtml', array(
        'methods' => 'POST',
        'callback' => 'getItemHtml',
    ) );
});
/**
 * Save dashboard items for a user
 */

function saveDashboard() {
    // this is json
    $dashboard = json_encode($_POST["dashboardItems"]);
    error_log($dashboard);
    // save to db
    $currentUser = wp_get_current_user();
    update_option("osDashboard_" . $currentUser->user_login, $dashboard);
}
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'dashboard/savedashboard', array(
        'methods' => 'POST',
        'callback' => 'saveDashboard',
    ) );
});