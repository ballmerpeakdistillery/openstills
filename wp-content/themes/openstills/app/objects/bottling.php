<?php
/********************************************************************
* Bottling
********************************************************************/


/**
 * Set all bottling meta on create
 * includes inventory for bottles
 * sets a final transfer
 * @param [type] $post [description]
 */
function setBottlingMeta($post) {

    // get all vessels
    $vessels = json_decode(get_post_meta($post->ID, "vessels", true));

    // add transfer
    foreach ($vessels as $vesselId => $value) {
        createTransferObject($vesselId, $post->ID, time());
    }

    // inventory
    $bottleId = getSingleMetaId($post->ID, "bottle-type");
    $qty = get_post_meta($post->ID, "bottle-count", true);
    $currentQty = get_post_meta($bottleId, "current-inventory", true);
    $newQty = $currentQty - $qty;
    update_post_meta($bottleId, "current-inventory", $newQty);

}

add_action('rest_after_insert_bottling', 'setBottlingMeta', 10, 3);