<?php
/********************************************************************
* Ferment functions
********************************************************************/

function fermentScripts() {
    // only do these on mash post types
    if (get_post_type() == "ferment") {
        wp_enqueue_script("charts", "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js");
        wp_enqueue_script("ferment-scripts", get_template_directory_uri() . '/js/ferment.js', array('jquery', 'charts'));
    }
}
add_action("wp_enqueue_scripts", "fermentScripts");


/**
 * Sets default values for a ferment
 * makes sure start time matches with mash end time
 */
function setDefaultFermentValues($post) {

    // is this a new post
    if (get_post_meta($post->ID, "save-status", true) != "new") {
        return;
    }
    update_post_meta($post->ID, "save-status", "existing");


    // get the mash that we're using
    $mashId = getSingleMetaId($post->ID, "mash");
    // set to archived
    update_post_meta($mashId, "status", "archived");

    $stopTime = get_post_meta($mashId, "stop-time", true);

    // set default values
    update_post_meta($post->ID, "start-time", $stopTime);
    update_post_meta($post->ID, "is-active", true);
    update_post_meta($post->ID, "status", "active");

    // create initial transfer object
    createTransferObject($mashId, $post->ID, time());

}
add_action( 'rest_after_insert_ferment', 'setDefaultFermentValues', 10, 3 );


/**
 * Stopping the ferment
 * this sets a few variables
 */

function stopFerment() {
    $fermentId = $_POST["id"];
    // setting active
    update_post_meta($fermentId, "is-active", false);
    update_post_meta($fermentId, "status", "ended");
    // setting start time
    update_post_meta($fermentId, "stop-time", time());
}

/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'ferment/stopferment', array(
        'methods' => 'POST',
        'callback' => 'stopFerment',
    ) );
});