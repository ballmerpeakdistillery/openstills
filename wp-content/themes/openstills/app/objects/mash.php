<?php

/********************************************************************
* Inventory
* Create debits and credits for post types
********************************************************************/


function mashScripts() {
    // only do these on mash post types
    if (get_post_type() == "mash") {
    }
}
add_action("wp_enqueue_scripts", "mashScripts");

/*
function mashCreditDebit($post) {

    // is this a new post
    if (get_post_meta($post->ID, "save-status", true) != "new") {
        // return false;
    }

    // get recipe that's being used
    // initially in json format
    $recipe = json_decode(get_post_meta($post->ID, "recipe", true));
    $recipe = $recipe[0];

    // get all recipe quantities for that recipe
    $metaExtra = get_post_meta($recipe, "meta-extra-fields", true);
    error_log($metaExtra);
    $metaExtra = json_decode($metaExtra, ture);

    foreach ($metaExtra as $supply => $qty) {
        error_log($supply . ": " . $qty);
    }


    update_post_meta($post->ID, "save-status", "saved");

}
add_action( 'rest_after_insert_mash', 'mashCreditDebit', 10, 3 );
*/


/**
 * Sets default values for a mash
 */
function setDefaultMashValues($post) {

    // is this a new post
    if (get_post_meta($post->ID, "save-status", true) != "new") {
        return;
    }
    update_post_meta($post->ID, "save-status", "existing");

    // set default values
    update_post_meta($post->ID, "is-active", true);
    update_post_meta($post->ID, "status", "new");

}
add_action( 'rest_after_insert_mash', 'setDefaultMashValues', 10, 3 );



/**
 * Starting the mash
 * this sets a few variables
 * it also subtracts the ingredients from inventory
 */

function startMash() {
    $mashID = $_POST["id"];
    // setting active
    update_post_meta($mashID, "is-active", true);
    update_post_meta($mashID, "status", "active");
    // setting start time
    update_post_meta($mashID, "start-time", time());

    // recipe stuff
    // NOTE: this next bit is to be compatiable with < 7.x
    // in the future replace this with array_key_first()
    $recipeId = getSingleMetaId($mashID, "recipe");

    // update ingredient qty
    $ingredients = json_decode(get_post_meta($recipeId, "ingredients", true));
    foreach ($ingredients as $ingredientId => $ingredient):
        $qty = $ingredient->qty;
        $currentQty = get_post_meta($ingredientId, "current-inventory", true);
        $newQty = $currentQty - $qty;
        update_post_meta($ingredientId, "current-inventory", $newQty);
    endforeach;

}

/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'mash/startmash', array(
        'methods' => 'POST',
        'callback' => 'startMash',
    ) );
});


/**
 * Stopping the mash the mash
 * this sets a few variables
 */

function stopMash() {
    $mashID = $_POST["id"];
    // setting active
    update_post_meta($mashID, "is-active", false);
    update_post_meta($mashID, "status", "ended");
    // setting start time
    update_post_meta($mashID, "stop-time", time());
}

/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'mash/stopmash', array(
        'methods' => 'POST',
        'callback' => 'stopMash',
    ) );
});
