<?php

/********************************************************************
* Notifications
********************************************************************/

/**
 * Define possible subscriptions
 */

function getSubscriptionList() {
    $subscriptionList = array(
        "base"  =>          array(
            array(
                "label"         => "Contact",
                "options"       => array(
                    "Create"        => "contact/create",
                    "Update"        => "contact/update",
                    "Status Change" => "contact/status"
                )
            ),
            array(
                "label"         => "Supply",
                "options"       => array(
                    "Low Stock"        => "supply/lowstock"
                )
            )
        ),
        "production"  =>          array(
            array(
                "label"         => "Mash",
                "options"       => array(
                    "Create"        => "mash/create",
                    "Update"        => "mash/update",
                    "Status Change" => "mash/status"
                )
            ),
            array(
                "label"         => "Ferment",
                "options"       => array(
                    "Create"        => "ferment/create",
                    "Update"        => "ferment/update",
                    "Status Change" => "ferment/status"
                )
            ),
            array(
                "label"         => "Run",
                "options"       => array(
                    "Create"        => "run/create",
                    "Update"        => "run/update",
                    "Status Change" => "run/status",
                    "Heads Cut"     => "run/headscut",
                    "Hearts Cut"    => "run/heartscut",
                )
            )
        )
    );

    return $subscriptionList;
}


/*
* Create post types and taxonomies
*/
function createNotificationPostType() {

    register_post_type('notification',
        array(
            'labels' => array(
                'name'                  => __('Notification'),
                'singular_name'         => __('Notification')
            ),
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => array('slug' => 'notification'),
            'supports'      => array('title', 'editor', 'custom-fields', 'excerpt', 'thumbnail', 'author')
        )
    );
    flush_rewrite_rules();
}

add_action('init', 'createNotificationPostType');


/*
* Creating notification post statuses
 */
function notificationPostStatuses(){
    register_post_status('unread', array(
        'label'                     => _x('Unread', 'post'),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'post_type'                 => array('notification'),
        'label_count'               => _n_noop('Unread <span class="count">(%s)</span>', 'Unread <span class="count">(%s)</span>'),
    ));
    register_post_status('read', array(
        'label'                     => _x('Read', 'post'),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'post_type'                 => array('notification'),
        'label_count'               => _n_noop('Read <span class="count">(%s)</span>', 'Read <span class="count">(%s)</span>'),
    ));
}
add_action('init', 'notificationPostStatuses');


/*
* Adding a notification
 */

function addNotification($action, $title, $message, $notificationUsers = false) {

    // get users for this action
    if (!$notificationUsers) {
        $notificationUsers = getActionSubscribers($action);
    }

    if (!$notificationUsers) {
        return;
    }

    // add it for these users
    foreach ($notificationUsers as $user) {
        // if we're the user adding this, then don't do it
        // we don't want a notification that we just created a post
        // we know that we created it
        if ($user == get_current_user_id()) {
            continue;
        }

        // insert the post
        $args = array(
            "post_type"     => "notification",
            "post_status"   => "unread",
            "post_title"    => $title,
            "post_content"  => $message,
            "post_name"     => createId(),
            "post_author"   => $user
        );
        wp_insert_post($args);
    }
}


/**
 * Getting notifications for a user
 */
function getUserNotifications() {
    $args = array(
        "post_type"     => "notification",
        "post_status"   => "unread",
        "author"   => get_current_user_id()
    );
    $posts = get_posts($args);

    return $posts;
}

function getUserNotificationCount() {
    $notifications = getUserNotifications();
    return count($notifications);
}

function getNotificationHtml() {

    // get notifications
    $notifications = getUserNotifications();

    // put to output buffer
    ob_start();
    foreach ($notifications as $notification) {
        $templateList = array(
            "templates/notification-" . $notification->post_type . ".php",
            "templates/notification.php"
        );
        $template = locate_template($templateList, false, false);
        include($template);
        //get_template_part('templates/notification', $notification->post_type);
    }
    $html = ob_get_clean();
    return array(
        "html" => $html
    );
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'notification/getnotificationhtml', array(
        'methods' => 'POST',
        'callback' => 'getNotificationHtml',
    ) );
});

/********************************************************************
* General notifications that exist for most post types
* ******************************************************************/


/**
* Notification on post creation
 */
function newPostNotification($post, $request, $creating) {
    // only on new posts
    if (!$creating) {
        return;
    }

    addNotification($post->post_type . "/create", "A new " . $post->post_type . " has been created", "new message");
}
$postTypes = getDefinedObjects();
foreach ($postTypes as $postType):
    // we don't want to put us in to an endless loop here
    // don't do this for notifications
    if ($postType == "notification") {
        continue;
    }
    add_action( 'rest_after_insert_' . $postType, 'newPostNotification', 10, 3 );
endforeach;


/**
* Notification on post update
*/
function postUpdateNotification($post_id, $post, $update) {

    // not on notifications
    if ($post->post_type == "notification") {
        return;
    }
    addNotification($post->post_type . "/update", $post->post_title . "has been updated", "new message");
}
add_action( 'save_post', 'postUpdateNotification', 10, 3 );

/********************************************************************
* Subscriptions
* ******************************************************************/

/**
 * Add and remove user subscriptions
 */
function addSubscription() {
    add_user_meta(get_current_user_id(), "subscription", $_POST["item"]);
}
function removeSubscription() {
    delete_user_meta(get_current_user_id(), "subscription", $_POST["item"]);
}


/**
 * get list of IDs for that actions subscribers
 */
function getActionSubscribers($action) {
    $users = get_users(array(
        'meta_key'     => "subscription",
        'meta_value'   => $action,
        'meta_compare' => "="
    ));
    // only return the id
    $userIds = array();
    foreach ($users as $user) {
        $userIds[] = $user->ID;
    }
    return $userIds;
}


/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'notification/addsubscription', array(
        'methods' => 'POST',
        'callback' => 'addSubscription',
    ) );
});
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'notification/removesubscription', array(
        'methods' => 'POST',
        'callback' => 'removeSubscription',
    ) );
});


/**
 * Marking notifications as read
 */

function markNotificationAsRead() {
    $notificationId = $_POST["id"];
    $args = array(
        "ID"            => $notificationId,
        "post_status"   => "read"
    );
    wp_update_post($args);
}
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'notification/markasread', array(
        'methods' => 'POST',
        'callback' => 'markNotificationAsRead',
    ) );
});