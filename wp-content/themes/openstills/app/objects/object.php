<?php
/********************************************************************
* Object functionality
* ******************************************************************/

/********************************************************************
* Object information functions
* ******************************************************************/

/**
 * Get a list of object post types by file
 * @return array list of object types
 */
function getDefinedObjects() {
    $objects = array();
    // auto load all config files
    $files = glob(get_template_directory() . "/objects/**/*.json");
    foreach ($files as $file) {
        // get post type;
        $fileParts = explode("/", $file);
        $postType = str_replace("-config.json", "", end($fileParts));

        $objects[] = $postType;
    }

    return $objects;
}

function getDefintedObjectsByNamespace() {
    $objects = array();
    // auto load all config files
    $files = glob(get_template_directory() . "/objects/**/*.json");
    foreach ($files as $file) {
        // get post type;
        $fileParts = explode("/", $file);
        $namespace = $fileParts[count($fileParts) - 2];
        $postType = str_replace("-config.json", "", end($fileParts));

        // check if namespace is already defined
        if (!array_key_exists($namespace, $objects)) {
            $objects[$namespace] = array();
        }

        // put post type in correct one
        $objects[$namespace][] = $postType;

    }

    return $objects;
}

function getDefinedNamespaces() {
    $directories = array_filter(glob(get_template_directory() . "/objects/*"), "is_dir");
    $returnDirectories = array();
    foreach ($directories as $directory):
        $returnDirectories[] = end(explode("/", $directory));
    endforeach;
    return $returnDirectories;
}


function getObjectConfig($namespace, $object) {

    $file = get_template_directory() . "/objects/" . $namespace . "/" . $object . "-config.json";
    $json = json_decode(file_get_contents($file), true);

    return $json;
}

/********************************************************************
* Create all available objects
* ******************************************************************/

/**
 * Take the JSON that gets submitted and save
 */
function saveObject() {
    // all of the $_POST stuff should be formatted exactly how we already want them
    // more or less we want to take that and output json to a file
    // for the record, PHP evaluating booleans as strings is bullshit
    // have to do all this extra strip slashes crap
    $data = json_decode(stripslashes($_POST["json"]), true);

    // create the file
    $postType = strtolower(str_replace(" ", "-", $data["config"]["labels"]["singular_name"]));
    $namespace = $data["namespace"];
    $fileName = get_template_directory() . "/objects/" . $namespace . "/" . $postType . "-config.json";
    try {
        $fileHandle = fopen($fileName, 'w');
        fwrite($fileHandle, json_encode($data));
    } catch (exception $e) {
        // error out something
    }
}

/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'saveobject', array(
        'methods' => 'POST',
        'callback' => 'saveObject',
    ) );
});

/**
 * Create all post types for saved config data
 * @return [type] [description]
 */
function createObjectPostTypes() {

    // auto load all config files
    $files = glob(get_template_directory() . "/objects/**/*.json");
    foreach ($files as $file) {
        // read in the data
        $json = json_decode(file_get_contents($file), true);

        $config = $json["config"];

        // add default supports
        $config["supports"] = array('title', 'editor', 'custom-fields', 'excerpt', 'thumbnail', 'author', 'comments');
        $config["has_archive"] = true;

        // get post type;
        $fileParts = explode("/", $file);
        $postType = str_replace("-config.json", "", end($fileParts));

        // register post type
        $results = register_post_type($postType, $config);

        /**
         * Register the meta for this post type
         */

        $metaGroups = $json["meta"];
        if ($metaGroups) {
            foreach ($metaGroups as $metaGroup):
                foreach ($metaGroup as $metaField):
                    register_meta('post', $metaField["slug"], array(
                        'show_in_rest'   => true,
                        'object_subtype' => $postType,
                        'type'           => "string",
                        'single'         => true
                    ));
                endforeach;
            endforeach;
        }

        // make sure to also register extra meta fields
        register_meta('post', "meta-extra-fields", [
            'show_in_rest'   => true,
            'object_subtype' => $postType,
        ]);
        register_meta('post', "save-status", [
            'show_in_rest'   => true,
            'object_subtype' => $postType,
        ]);

    }
    flush_rewrite_rules();
}
add_action('init', 'createObjectPostTypes');

function createObjectTaxonomies() {
    $files = glob(get_template_directory() . "/objects/**/*.json");
    foreach ($files as $file):
        $json = json_decode(file_get_contents($file), true);
        // get post type;
        $fileParts = explode("/", $file);
        $postType = str_replace("-config.json", "", end($fileParts));

        if (!array_key_exists("taxonomy", $json)) {
            continue;
        }
        $taxonomies = $json["taxonomy"];

        foreach ($taxonomies as $taxonomy):
            register_taxonomy(
                $postType . "-" . $taxonomy["slug"],
                array($postType),
                array(
                    'label'                 => __($taxonomy["label"]),
                    'rewrite'               => array('slug' => $taxonomy["slug"]),
                    'hierarchical'          => true,
                    'show_in_rest'          => true,
                    'rest_base'             => 'category',
                    'rest_controller_class' => 'WP_REST_Terms_Controller'
                )
            );
        endforeach;
    endforeach;
}
add_action('init', 'createObjectTaxonomies');


/********************************************************************
* Add query vars for creating and editing objects
* ******************************************************************/

function addObjectQueryVars($addedVars) {
    $addedVars[] = "namespace";
    $addedVars[] = "object";
    return $addedVars;
}
add_filter('query_vars', 'addObjectQueryVars');

function addObjectRewriteRule($addedRules) {
    $addedNewRules = array(
        'settings/object/add/?$' => 'index.php?pagename=add-edit-object',
        'settings/object/edit/([^/]+)/([^/]+)/?$' => 'index.php?pagename=add-edit-object&namespace=$matches[1]&object=$matches[2]'
    );
    $addedRules = $addedNewRules + $addedRules;
    return $addedRules;
}
add_filter('rewrite_rules_array', 'addObjectRewriteRule');

/**
 * Adding terms to a taxonomy
 */

function addTermToTaxonomy() {
    $results = wp_insert_term($_POST["term_name"], $_POST["taxonomy_name"]);
    return $results;
}

/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'createterm', array(
        'methods' => 'POST',
        'callback' => 'addTermToTaxonomy',
    ) );
});

/********************************************************************
* Namespaces
* ******************************************************************/

function createNamespace() {
    $fullDir = get_template_directory() . "/objects/" . $_POST["namespace"];
    mkdir($fullDir, 755);
}

/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'createnamespace', array(
        'methods' => 'POST',
        'callback' => 'createNamespace',
    ) );
});


/********************************************************************
* Helpers
* ******************************************************************/

/**
 * Gets a single ID from a post list
 * When using post type as meta, it formats it weird
 * This just helps us get that first ID in the array
 * @param  int      $id  post ID for the meta
 * @param  string   $key meta key
 * @return int      post ID
 */
function getSingleMetaId($id, $key) {
    $metaJson = json_decode(get_post_meta($id, $key, true), true);
    $metaId = array_keys($metaJson)[0];
    return $metaId;
}


/**
 * Getting stats
 */
function getMetaLists() {

    // create empty array
    $returnArray = array();

    // get key values
    $keys = $_POST["keys"];
    foreach ($keys as $key) {
        $returnArray[$key] = get_post_meta($_POST["ID"], $key, true);
    }

    return $returnArray;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'object/getmeta', array(
        'methods' => 'POST',
        'callback' => 'getMetaLists',
    ) );
});