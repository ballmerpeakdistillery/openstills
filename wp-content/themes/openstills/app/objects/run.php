<?php
/********************************************************************
* Run functions
* ******************************************************************/

function runScripts() {
    // only do these on mash post types
    if (get_post_type() == "run") {
        wp_enqueue_script("charts", "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js");
        wp_enqueue_script("run-scripts", get_template_directory_uri() . '/js/run.js', array('jquery', 'charts'));
    }
}
add_action("wp_enqueue_scripts", "runScripts");


/**
 * Save initial data, create transfers
 */
function setRunMeta($post) {

    // set status
    update_post_meta($post->ID, "status", "new");

    // get run type
    $type = get_post_meta($post->ID, "type", true);
    // set the transfer
    switch ($type) {
        case "stripping":
            $ferment = getSingleMetaId($post->ID, "ferment");
            createTransferObject($ferment, $post->ID, time());
            break;
        case "spirit":
            // create the transfer
            // we're looking for the transfer from the mash we've selected
            // that will give us the vessel that the low wines are currently in
            $strippingRun = getSingleMetaId($post->ID, "stripping-run");
            // there's a possibility that this wasn't an actual stripping run
            // we're going to check against the "stripping run" type
            $strippingRunType = get_post_meta($strippingRun, "type", true);
            if ($strippingRunType == "spirit") {
                // we need to get the heads vessel
                $fromVessel = getSingleMetaId($strippingRun, "hearts-collection", true);
            } else if ($strippingRunType == "stripping") {
                // we need to just get the end vessel
                $fromVessel = getSingleMetaId($strippingRun, "collection-vessel", true);
            }
            createTransferObject($fromVessel, $post->ID, time());
            break;
    }

}
add_action('rest_after_insert_run', 'setRunMeta', 10, 3);


/**
 * Start run
 */

function startRun() {

    $runId = $_POST["id"];
    // setting active
    update_post_meta($runId, "is-active", true);
    update_post_meta($runId, "status", "active");
    // setting start time
    update_post_meta($runId, "start-time", time());


    // check for flavor run
    $recipe = get_post_meta($runId, "recipe", true);
    if (strlen($recipe) > 0) {
        $recipeId = getSingleMetaId($runId, "recipe");

        // update ingredient qty
        $ingredients = json_decode(get_post_meta($recipeId, "ingredients", true));
        foreach ($ingredients as $ingredientId => $ingredient):
            $qty = $ingredient->qty;
            $currentQty = get_post_meta($ingredientId, "current-inventory", true);
            $newQty = $currentQty - $qty;
            update_post_meta($ingredientId, "current-inventory", $newQty);
        endforeach;
    }

}

add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'run/startrun', array(
        'methods' => 'POST',
        'callback' => 'startRun',
    ) );
});



/**
 * Ending run
 */

function stopRun() {

    $runId = $_POST["id"];
    // setting active
    update_post_meta($runId, "is-active", false);
    update_post_meta($runId, "status", "ended");
    // setting start time
    update_post_meta($runId, "stop-time", time());

    // make the transfers
    $type = get_post_meta($_POST["id"], "type", true);
    switch ($type) {
        case "stripping":
            // this is transferring from the still to the collection vessel that was provided
            $collectionVessel = getSingleMetaId($_POST["id"], "collection-vessel");
            createTransferObject($runId, $collectionVessel, time());
            break;
        case "spirit":
            // need to get all three vessels
            $headsCollection = getSingleMetaId($_POST["id"], "heads-collection");
            createTransferObject($runId, $headsCollection, time());
            $heartsCollection = getSingleMetaId($_POST["id"], "hearts-collection");
            createTransferObject($runId, $heartsCollection, time());
            $tailsCollection = getSingleMetaId($_POST["id"], "tails-collection");
            createTransferObject($runId, $tailsCollection, time());
            break;
    }

}

add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'run/stoprun', array(
        'methods' => 'POST',
        'callback' => 'stopRun',
    ) );
});