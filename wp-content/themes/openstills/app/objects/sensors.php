<?php
/********************************************************************
* Sensor functions
* ******************************************************************/


/**
 * Get all sensor IDs and labels that are assigned to a object
 * @param  int $vessel   post ID of the vessel
 * @return array         associative array of ID => Label
 */
function getSensors($vessel) {

    // get meta value, decode from json
    $sensors = json_decode(get_post_meta($vessel, "sensors", true));

    // get labels for all of them, assign to return array
    $returnArray = array();
    foreach ($sensors as $id => $notNeeded) {
        $label = get_post_meta($id, "label", true);
        $returnArray[$id] = $label;
    }

    // return it
    return $returnArray;

}


/**
 * Gets the post ID of the active post with this sensor
 * @param  int $id sensor ID
 * @return int     Post ID
 */
function getSensorPost($id) {

    // get all active posts
    $args = array(
        'post_type'  => 'any',
        'meta_query' => array(
            array(
                'key' => 'is-active',
                'value' => true,
                'compare' => '=',
            )
        )
    );
    $posts = get_posts($args);

    // run through and get all sensors on active posts
    foreach ($posts as $post) {
        $vessel = getSingleMetaId($post->ID, "vessel");
        $sensors = getSensors($vessel);

        // if sensor is in this post, return the ID
        foreach ($sensors as $sensorId => $sensorLabel) {
            // post ID isn't the same as the given ID
            $sensorStringId = get_post_meta($sensorId, "sensor-id", true);
            if ($sensorStringId == $id) {
                return $post->ID;
            }
        }
    }

    // return false if there's no active posts for this sensor
    return false;
}

/**
 * Adds data to active post associated with that sensor
 */
function addDataPoint() {

    // get base info
    // check if we're doing a get or post
    if (isset($_GET["id"])) {
        $id = $_GET["id"];
        $data = $_GET["data"];
    } else if (isset($_POST["id"])) {
        $id = $_POST["id"];
        $data = $_POST["data"];
    } else {
        // nothing is set
        return;
    }

    // find active post
    $toAddId = getSensorPost($id);

    // get current sensor data from post
    $sensorKey = "sensor-" . $id;
    $sensorJson = json_decode(get_post_meta($toAddId, $sensorKey, true), true);

    // add data with time key
    $sensorJson[time()] = $data;

    // add it back in
    update_post_meta($toAddId, $sensorKey, json_encode($sensorJson));

}

/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'sensor/adddata', array(
        'methods' => array('GET', 'POST'),
        'callback' => 'addDataPoint',
    ) );
});


/**
 * Gets sensor data for the sensor-id and the active post
 * @return [type] [description]
 */
function getSensorData() {

    // get base info
    $sensorId = $_GET["sensor-id"];
    $sensorIdString = get_post_meta($sensorId, "sensor-id", true);
    $postId = $_GET["post-id"];

    // get the meta
    $sensorData = json_decode(get_post_meta($postId, "sensor-" . $sensorIdString, true), true);

    return $sensorData;

}
/**
 * Create the API route for this function
 */
add_action( 'rest_api_init', function () {
    register_rest_route( 'openstills', 'sensor/getdata', array(
        'methods' => 'GET',
        'callback' => 'getSensorData',
    ) );
});
