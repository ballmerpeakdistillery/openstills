<?php
/********************************************************************
* Supplies
********************************************************************/

function createLowSupplyNotification($meta_id, $object_id, $meta_key, $_meta_value) {

    // only run on current inventory
    if ($meta_key != "current-inventory") {
        return;
    }

    // check if we're below the notification threshold
    $threshold = get_post_meta($object_id, "notification-threshold", true);
    if ($_meta_value < $threshold) {
        // check if we've already set a notification
        if (get_post_meta($object_id, "status", true) == "low-stock") {
            return;
        }

        // set that low stock status
        update_post_meta($object_id, "status", "low-stock");

        // get post info
        $ingredient = get_post($object_id);

        $title = "You're running low on " . $ingredient->post_title;
        $message = "There is only " . $_meta_value . " " . get_post_meta($ingredient->ID, "supply-unit", true) . " left in inventory";
        addNotification("supply/lowstock", $title, $message);
    }

}

add_action("updated_post_meta", "createLowSupplyNotification", 10, 4);