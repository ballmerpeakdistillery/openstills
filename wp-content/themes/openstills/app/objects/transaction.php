<?php


/**
 * Calculate total transaction value when a transaction is inserted
 * @param  obj $post post object that we're dealing with
 */
function transactionAdded($post) {

    // get current value
    $currentBank = get_option('osAccount_balance');
    // if this is the first transaction, start at 0
    if (!$currentBank) {
        $currentBank = 0;
    }
    // make sure we're dealing with a float not a string
    $currentBank = floatval($currentBank);

    // check if credit or debit and calculate
    $amount = floatval(get_post_meta($post->ID, "amount", true));
    $type = get_post_meta($post->ID, "type", true);
    if ($type == "credit") {
        $currentBank = $currentBank + $amount;
    } else {
        $currentBank = $currentBank - $amount;
    }

    // update option
    update_option('osAccount_balance', $currentBank);
}

add_action( 'rest_after_insert_transaction', 'transactionAdded', 10, 3 );