<?php
/********************************************************************
* Reports
* ******************************************************************/

function createReportPostType() {

    register_post_type('report',
        array(
            'labels' => array(
                'name'                  => __('Report'),
                'singular_name'         => __('Report')
            ),
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => array('slug' => 'transfer'),
            'supports'      => array('title', 'editor', 'custom-fields', 'excerpt', 'author'),
            'show_in_rest'  => true
        )
    );
    flush_rewrite_rules();
}

add_action('init', 'createReportPostType');


/**
 * Trace a bottling/batch/whatever back through all transfers
 * @param int $bottling post id of the bottling
 */

function getTrace($bottling) {

    // get the transfer that has this bottling as the "to"
    $args = array(
        "post_type"     => "transfer",
        "meta_query"    => array(
            array(
                "key"       => "to",
                "value"     => $bottling
            )
        )
    );
    $endTransfer = get_posts($args);
    if (count($endTransfer) > 0) {
        $endTransfer = $endTransfer[0]->ID;
    }

    // work our way back
    // start at what we have
    $transfer = $endTransfer;
    $transferList = array(
        $transfer
    );
    for ($i = 0; $i < 100; $i++) {
        // we're going to do it this way just to avoid the possibility of an endless loop
        $previous = get_post_meta(end($transferList), "previous", true);
        $transferList[] = $previous;

        // if we've reached the end
        if ($previous == 0) {
            $i = 100;
        }
    }

    // return it in reverse so it's from start to finish
    return array_reverse($transferList);

}