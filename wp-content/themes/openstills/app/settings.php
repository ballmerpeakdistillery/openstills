<?php
/********************************************************************
* Settings
********************************************************************/

function openstills_nav() {

    // include base menu
    get_template_part('templates/menu/menu', 'base');

    // include user specific menu
    $currentUser = wp_get_current_user();
    $userMenu = get_option("osMenu_" . $currentUser->user_login);

    if ($userMenu):
        foreach ($userMenu as $key => $group):
        ?>
        <p class="menu-label"><?php echo $group["label"]; ?></p>
        <ul class="menu-list">
            <?php foreach ($group["items"] as $key => $groupData): ?>
                <?php
                switch ($groupData["type"]) {
                    case "object":
                        ?>
                        <li class="menu-item" data-namespace="<?php echo $groupData["namespace"]; ?>" data-object="<?php echo $groupData["object"]; ?>" data-type="object">
                            <div class="columns">
                                <div class="column is-6">
                                    <?php echo $groupData["object"]; ?>
                                </div>
                                <div class="column is 6 menu-links">
                                    <?php if (in_array("add", $groupData["links"])): ?>
                                        <a href="/add/<?php echo $groupData["namespace"] . "/" . $groupData["object"] . "/"; ?>">
                                            <i class="fa fa-plus" data-item="add"></i>
                                        </a>
                                    <?php endif; ?>
                                    <?php if (in_array("archive", $groupData["links"])): ?>
                                        <a href="/<?php echo $groupData["rewrite"]; ?>">
                                            <i class="fa fa-list" data-item="archive"></i>
                                        </a>
                                    <?php endif; ?>
                                    <?php if (in_array("settings", $groupData["links"])): ?>
                                        <a href="/settings/object/edit/<?php echo $groupData["namespace"] . "/" . $groupData["object"]; ?>">
                                            <i class="fa fa-gears" data-item="settings"></i>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </li>
                        <?php
                        break;
                    case "link":

                        break;
                }
                ?>
            <?php endforeach; ?>
        </ul>
        <?php
        endforeach;
    else:
        echo '<p class="menu-label">Go to Settings->Menus to get started</p>';
    endif;

    // include settings menu
    get_template_part('templates/menu/menu', 'settings');
}