<?php
/********************************************************************
* Task functions
********************************************************************/


/**
 * Set up routes for tasks
 * should be base/tasks/[username]
 */


function addTaskQueryVars($addedVars) {
    $addedVars[] = "username";
    return $addedVars;
}
add_filter('query_vars', 'addTaskQueryVars');

function addTaskRewriteRules($addedRules) {
    $addedNewRules = array(
        'tasks/([^/]+)/?$' => 'index.php?pagename=tasks&username=$matches[1]',
    );
    $addedRules = $addedNewRules + $addedRules;
    return $addedRules;
}
add_filter('rewrite_rules_array', 'addTaskRewriteRules');


/**
 * Get new task count for user
 * @return int|string returns count of new tasks or "10+" for any number over 10
 */
function getTaskCount($status = "new") {

    // get tasks with status of new and asignee of the current user
    $currentUser = wp_get_current_user();
    $args = array(
        "post_type"     => "task",
        "meta_query"    => array(
            array(
                "key"       => "status",
                "value"     => $status
            ),
            array(
                "key"       => "asignee",
                "value"     => $currentUser->ID
            )
        )
    );
    $tasks = get_posts($args);

    // if it's more than 10 just say 10+
    $taskCount = count($tasks);
    if ($taskCount == 11) {
        $taskCount = "10+";
    }

    return $taskCount;

}


/**
 * Get user's tasks
 */

function getMyTasks() {
    // get tasks with status of new and asignee of the current user
    $currentUser = wp_get_current_user();
    $args = array(
        "post_type"         => "task",
        "posts_per_page"    => -1,
        "meta_query"        => array(
            array(
                "key"           => "asignee",
                "value"         => $currentUser->ID
            )
        )
    );
    $tasks = get_posts($args);

    return $tasks;
}


/**
 * Notify user when a task gets assigned to them
 */
function addTaskAsigneeNotification($task, $user) {
    $title = "A task has been assigned to you";
    $message = 'You have been assigned to the task <a href="' . get_the_permalink($task) . '">' . get_the_title($task) . "</a>";
    addNotification("", $title, $message, array($user));
}

function postCreatedCheckAsignee($post, $request, $creating) {
    $asignee = get_post_meta($post->ID, "asignee", true);
    addTaskAsigneeNotification($post->ID, $asignee);
}
add_action('rest_after_insert_task', 'postCreatedCheckAsignee', 10, 3);