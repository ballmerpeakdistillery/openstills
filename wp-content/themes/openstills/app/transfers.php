<?php
/********************************************************************
* Transfers
* ******************************************************************/


/**
 * Create the post types
 */

/*
function createTransferPostType() {

    register_post_type('transfer',
        array(
            'labels' => array(
                'name'                  => __('Transfer'),
                'singular_name'         => __('Transfer')
            ),
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => array('slug' => 'transfer'),
            'supports'      => array('title', 'editor', 'custom-fields', 'excerpt', 'author'),
            'show_in_rest'  => true
        )
    );
    flush_rewrite_rules();
}

add_action('init', 'createTransferPostType');
*/

/**
 * Creating a line of transfers to keep track of where everything goes
 * @param  int $from   post ID of mash, ferment, run, or vessel that this is going from
 * @param  int $to     post ID of mash, ferment, run or vessel that this is going to
 * @param  int $date   timestamp of when this transfer is happenieng
 */
function createTransferObject($from, $to, $date) {
    $args = array(
        "post_type"     => "transfer",
        "post_title"    => createId(),
        "post_status"   => "publish"
    );
    $transferId = wp_insert_post($args);

    // insert the meta
    add_post_meta($transferId, "from", $from);
    add_post_meta($transferId, "to", $to);
    add_post_meta($transferId, "time", $date);

    // set previous transfer
    $fromType = get_post_type($from);
    if ($fromType == "vessel") {
        // vessel's wont be unique post IDs
        // so we have to keep track of what the transfer ID is on them
        $previous = get_post_meta($from, "current_transfer", true);
    } else {
        // mashes, runs, ferments will all be unique
        // so we just have to get the ID where to is that post ID
        $args = array (
            "post_type"     => "transfer",
            "meta_query"    => array(
                array (
                    "key"       => "to",
                    "value"     => $from
                )
            )
        );
        $previousTransfer = get_posts($args);
        if (count($previousTransfer) > 0) {
            // this hsould always be the case
            // except for mashes
            $previous = $previousTransfer[0]->ID;
        } else {
            $previous = 0;
        }
    }
    add_post_meta($transferId, "previous", $previous);

    // if to type is vessel, set the current_transfer
    $toType = get_post_type($to);
    if ($toType == "vessel") {
        update_post_meta($to, "current_transfer", $transferId);
    }
}