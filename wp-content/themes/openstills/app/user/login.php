<?php
/********************************************************************
* Login functions
********************************************************************/


/**
 * Redirect all users to front end login page if they're not logged in
 * Note: Seems like this runs twice, should probably look in to that
 * Also note: I know that hardcoding 'login' isn't great
 */
function osForceLogin() {
    if (!is_admin() && !is_user_logged_in() && !is_page('login')) {
        wp_redirect(home_url("login"));
        exit();
    }
}
add_action("wp", "osForceLogin");

/**
 * When a user logs in, kick them to the dashboard instead of the default wp-admin page
 * @return string dashboard url (hopefully)
 */
function osLoginRedirect() {
    return home_url();
}
add_filter('login_redirect', 'osLoginRedirect', 10, 3);