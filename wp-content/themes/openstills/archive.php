<?php get_header(); ?>
<?php $postType = get_post_type_object(get_queried_object()->name); ?>

<header class="archive-header">
    <h1 class="title level">
        <span class="level-left">
            <?php echo $postType->labels->name; ?>
        </span>
        <a href="/add/<?php echo get_queried_object()->rewrite["slug"]; ?>/" class="button is-primary level-right">
            New <?php echo $postType->labels->singular_name; ?>
            <i class="fa fa-plus"></i>
        </a>
    </h1>
</header>
<!-- section -->
<section>
	<?php get_template_part('templates/archive', get_queried_object()->name); ?>

	<?php get_template_part('pagination'); ?>

</section>

<?php get_footer(); ?>
