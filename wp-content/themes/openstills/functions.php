<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: openstills.com | @openstills
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Localisation Support
    load_theme_textdomain('openstills', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

/*
// HTML5 Blank navigation
function dashboard_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'dashboard-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => new T5_Nav_Menu_Walker_Simple()
		)
	);
}

function configuration_nav()
{
    wp_nav_menu(
    array(
        'theme_location'  => 'configuration-menu',
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul>%3$s</ul>',
        'depth'           => 0,
        'walker'          => new T5_Nav_Menu_Walker_Simple()
        )
    );
}
*/

// Load HTML5 Blank scripts (header.php)
function openstills_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('openstillsscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('openstillsscripts'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function openstills_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    //wp_register_style('openstills', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    //wp_enqueue_style('openstills'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'dashboard-menu' => __('Dashboard Menu', 'openstills'), // Main Navigation
        'configuration-menu' => __('Configuration Menu', 'openstills'), // Main Navigation
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'openstills') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function openstillsgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function openstillscomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'openstills_header_scripts'); // Add Custom Scripts to wp_head
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'openstills_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'openstillsgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether



// end html5blank stuff
// begin openstills stuff



/********************************************************************
 * OpenStills theme
 *******************************************************************/

function osStylesScripts() {
    // lib styles
    wp_enqueue_style("bulma", get_template_directory_uri() . "/css/lib/bulma.min.css");
    wp_enqueue_style("slick-slider", get_template_directory_uri() . "/css/lib/slick.css");
    wp_enqueue_style("font-awesome", "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
    // lib scripts
    wp_enqueue_script("serializejson", get_template_directory_uri() . "/js/lib/jquery.serializejson.min.js", array("jquery"));
    wp_enqueue_script("jquery-ui-sortable");
    wp_enqueue_script("slick-slider", get_template_directory_uri() . "/js/lib/slick.min.js", array("jquery"));
    wp_enqueue_script("charts", "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js");
    wp_enqueue_script("mash-scripts", get_template_directory_uri() . '/js/mash.js', array('jquery', 'charts'));
    // int styles
    wp_enqueue_style("os-base", get_template_directory_uri() . "/css/base.css");
    wp_enqueue_style("os-header", get_template_directory_uri() . "/css/header.css");
    wp_enqueue_style("os-archive", get_template_directory_uri() . "/css/archive.css");
    wp_enqueue_style("os-add", get_template_directory_uri() . "/css/add.css");
    wp_enqueue_style("os-object", get_template_directory_uri() . "/css/object.css");
    wp_enqueue_style("os-page", get_template_directory_uri() . "/css/page.css");
    // int scripts
    wp_enqueue_script("os-base", get_template_directory_uri() . "/js/base.js", array("jquery"));
    wp_enqueue_script("os-add", get_template_directory_uri() . "/js/add.js", array("jquery", "os-base"));
    wp_enqueue_script("os-object", get_template_directory_uri() . "/js/object.js", array("jquery", "os-base"));
    wp_enqueue_script("os-menu", get_template_directory_uri() . "/js/menu.js", array("jquery", "os-base"));
    wp_enqueue_script("os-subscriptions", get_template_directory_uri() . "/js/subscriptions.js", array("jquery", "os-base"));
    wp_enqueue_script("os-notifications", get_template_directory_uri() . "/js/notifications.js", array("jquery", "os-base"));
    wp_enqueue_script("os-comments", get_template_directory_uri() . "/js/comments.js", array("jquery", "os-base"));
    wp_enqueue_script("os-calendar", get_template_directory_uri() . "/js/calendar.js", array("jquery", "os-base"));
    wp_enqueue_script("os-dashboard", get_template_directory_uri() . "/js/dashboard.js", array("jquery", "os-base"));
}
add_action("wp_enqueue_scripts", "osStylesScripts");

/********************************************************************
 * OpenStills functionality
 *******************************************************************/

include("app/base.php");
include("app/settings.php");
include("app/user/login.php");
include("app/user/menu.php");
include("app/objects/object.php");
include("app/objects/mash.php");
include("app/objects/notification.php");
include("app/objects/sensors.php");
include("app/objects/ferment.php");
include("app/objects/run.php");
include("app/tasks.php");
include("app/comments.php");
include("app/dashboard.php");
include("app/objects/supply.php");
include("app/transfers.php");
include("app/objects/bottling.php");
include("app/reports.php");
include("app/objects/transaction.php");










/**
 * Input rendering functions
 */

function getFieldClasses($field, $base = "") {
    $classes = array();

    // add any classes defined in the config
    if (array_key_exists("classes", $field["args"])) {
        $specifiedClasses = explode(" ", $field["args"]["classes"]);
        $classes = array_merge($classes, $specifiedClasses);
    }

    // add any required classes
    if (array_key_exists("required", $field["args"]) && $field["args"]["required"] == true) {
        $classes[] = "required";
    }

    $classes[] = $base;
    return implode(" ", $classes);
}
function getFieldAtts($field) {
    $attributes = array();

    // get any defined attributes
    if (array_key_exists("attributes", $field["args"])) {
        // should already be in array format
        $specifiedAttributes = $field["args"]["attributes"];
        $attributes = array_merge($attributes, $specifiedAttributes);
    }

    $attributeString = "";
    foreach ($attributes as $key => $value) {
        $attributeString .= $key . '="' . $value . '"';
    }

    return $attributeString;
}
function getWrapperClasses($field, $base = "") {
    $classes = array();

    // add any classes defined in the config
    if (array_key_exists("wrapperClasses", $field["args"])) {
        $specifiedClasses = explode(" ", $field["args"]["wrapperClasses"]);
        $classes = array_merge($classes, $specifiedClasses);
    }

    // add any required classes
    if (array_key_exists("required", $field["args"]) && $field["args"]["required"] == true) {
        $classes[] = "has-required";
    }

    $classes[] = $base;
    return implode(" ", $classes);
}
