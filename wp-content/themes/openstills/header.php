<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

        <!-- sticky nav -->
        <nav class="navbar">
            <div class="level">
                <div class="navbar-brand level-left">
                    <a href="/">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.svg" />
                    </a>
                </div>
                <?php if (is_user_logged_in()): ?>
                <div class="navbar-actions level-right">
                    <ul id="global-items">
                        <li data-count="0"><i class="fa fa-calendar"></i></li>
                        <li data-count="0"><i class="fa fa-envelope"></i></li>
                        <li id="notification-icon" data-count="<?php echo getUserNotificationCount(); ?>" data-action="notification/getnotificationhtml" data-callback="loadNotificationHtml">
                            <i class="fa fa-bell"></i>
                            <ul id="notification-list" class="box"></ul>
                        </li>
                    </ul>
                </div>
                <?php endif; ?>
            </div>
        </nav>

		<!-- container -->
		<div class="">
            <div class="columns is-fullheight">

                <?php if (is_user_logged_in()): ?>
    			<header class="header clear column is-3" role="banner" id="navigation-sidebar">
                    <!-- side navigation -->
    				<nav class="nav" role="navigation">
                        <?php openstills_nav(); ?>
    				</nav>
    			</header>

                <main role="main" class="column is-9">
                <?php else: ?>
                <main role="main" class="column is-12">
                <?php endif; ?>

                    <div class="messages"></div>
