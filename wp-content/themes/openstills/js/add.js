/********************************************************************
* Scripts related to adding and editing posts
* ******************************************************************/



/********************************************************************
* Form validation on post edit pages
* ******************************************************************/
jQuery(document).ready(function($) {

    // enable save if all post fields are filled out
    function validateThisForm() {
        // run validation function
        var results = validate(".post-edit-wrapper");
        // turn it back on by default
        $('.post-edit-wrapper [data-action="save-object"]').attr("disabled", true);

        // turn disabled off if everything looks good
        if (results) {
            $('.post-edit-wrapper [data-action="save-object"]').attr("disabled", false);
        }
    }

    // run validation function for all possible input changes
    $(".post-edit-wrapper input").on("input", function() {
        validateThisForm();
    });
    $(".post-edit-wrapper textarea").on('change keyup paste', function() {
        validateThisForm();
    });
    $(".post-edit-wrapper select").on("change", function() {
        validateThisForm();
    });

});

/********************************************************************
* Form submission on post edit pages
* ******************************************************************/
jQuery(document).ready(function($) {

    //$('[data-action="save-object"]').click(function() {
    openStills.saveObject = function(instance) {
        // button styles
        $(instance).addClass("is-loading");

        var senddata = $(instance).parents("form").serializeJSON();

        // add taxonomies
        var categories = Array();
        $(".taxonomy-checkbox:checked").each(function() {
            categories.push($(this).val());
        });
        senddata["category"] = categories;


        console.log(senddata);

        // set API url
        // if it's new, no post ID is specified
        // if it's edit, post ID is appended to the URL
        var apiUrl = "/wp-json/wp/v2/" + postType;
        if (postId) {
            apiUrl += "/" + postId;
        }

        // handle the API call
        $.post({
            beforeSend: (xhr) => {
                // set the nonce so we can validate
                xhr.setRequestHeader('X-WP-Nonce', nonceData.nonce);
            },
            url: apiUrl,
            type: 'POST',
            data: senddata,
            success: (response) => {
                console.log(response);
                // take off loading
                $(instance).removeClass("is-loading");

                // create messages html
                addSuccess("Your object has been saved!");

                // add post id value
                postId = response.id;
                var newPostTitle = response.title.rendered;

                // remove success message
                setTimeout(function() {
                    window.location.replace(response.guid.rendered);
                    // $("#post-save-success-message").slideUp(function() {
                    //     $(this).remove();
                    // });
                }, 1000);
            },
            error: (response) => {
                console.log(response);
                // create messages html
                addGenericError();

                // remove success message
                setTimeout(function() {
                    $("#post-save-error-message").slideUp(function() {
                        $(this).remove();
                    });
                }, 10000);
            }
        });
    };
});


/********************************************************************
* Editing terms
* ******************************************************************/

jQuery(document).ready(function($) {

    //$(".add-term").click(function() {
    openStills.addTerm = function(instance) {

        $(instance).addClass("loading");

        var taxonomyName = $(instance).parents(".taxonomy-wrapper").attr("data-taxonomy-name");
        var termName = $(instance).parents(".taxonomy-wrapper").find(".term-name-input").val();
        var senddata = {
            "term_name": termName,
            "taxonomy_name": taxonomyName
        };
        console.log(senddata);

        // handle the API call
        var apiUrl = "/wp-json/openstills/createterm";
        $.post({
            beforeSend: (xhr) => {
                // set the nonce so we can validate
                xhr.setRequestHeader('X-WP-Nonce', nonceData.nonce);
            },
            url: apiUrl,
            type: 'POST',
            data: senddata,
            success: (response) => {
                console.log(response);
                // take off loading
                $(instance).removeClass("is-loading");

                var html = "<li>";
                html += '<label class="checkbox">';
                html += '<input type="checkbox" name="category" value="' + response.term_id + '">';
                html += termName,
                html += '</label>';
                html += '</li>';

                $(instance).parents(".taxonomy-wrapper").find(".term-list").append(html);

                $(instance).removeClass(".is-loading");
            },
            error: (response) => {
                console.log(response);
                // create messages html
                addGenericError();

                // remove success message
                setTimeout(function() {
                    $("#post-save-error-message").slideUp(function() {
                        $(this).remove();
                    });
                }, 10000);
            }
        });
    };

});


/********************************************************************
* Load all of the post info into inputs on edit load
* ******************************************************************/

jQuery(document).ready(function($) {

    if (typeof postId !== 'undefined' && postId != false) {
        var apiUrl = "/wp-json/wp/v2/" + postType + "/" + postId;
        // handle the API call
        $.get({
            url: apiUrl,
            success: (response) => {
                console.log(response);
                // take off loading class

                // loop through all base values
                $.each(response, function(key, value) {
                    // check if it's taxonomy
                    // this is needed because all taxonomy names are a key
                    if (key == "category") {
                        return true;
                    }

                    // check if it's plain text or an object
                    var addVal = value;
                    if (typeof value == "object") {
                        addVal = value.rendered;
                    }

                    // decode html values
                    addVal = $("#htmldecoder").html(addVal).text();
                    $(".post-edit-wrapper").find('[name="' + key + '"]').val(addVal);
                });

                // loop through taxonomies
                $.each(response["category"], function(key, value) {
                    $('.taxonomy-checkbox[value="' + value + '"]').prop("checked", true);
                });

                // loop through meta
                $.each(response["meta"], function(metaKey, metaValue) {
                    var checkName = 'meta[' + metaKey + ']';
                    $('[name="' + checkName + '"]').val(metaValue);
                });

                // loop through all taxonomy values
                $.each(response["category"], function(key, value) {
                    $(".post-edit-wrapper").find('[name="category"][value="' + value +'"]').prop("checked", true);
                });
            },
            error: (response) => {
                console.log(response);
                // create messages html
                addGenericError();

                // remove success message
                setTimeout(function() {
                    $("#post-save-error-message").slideUp(function() {
                        $(this).remove();
                    });
                }, 10000);
            }
        });
    }

});


/********************************************************************
* Defaults for specific types
* ******************************************************************/

jQuery(document).ready(function($) {

    // get date string for most default formates
    var today = new Date();
    var dateString = (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear();

    // get object type
    var postType = $("#post_type").val();

    var defaultTtitle = "";
    // get default formats
    switch (postType) {
        case "ferment":
            // [recipe] fermet [date]
            
            break;
    }

});