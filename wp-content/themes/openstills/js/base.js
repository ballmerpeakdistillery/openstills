/********************************************************************
* Base functions for the site
* It's a lot like Frank's red hot
* we can put this shit on everything
* ******************************************************************/

// app namespace
var openStills = {};

/********************************************************************
* Page navigation
* ******************************************************************/

// tabs
jQuery(document).ready(function($) {

    $(".tab").click(function() {
        // get objects and variables
        var wrapper = $(this).parents(".tabs-wrapper");
        var target = $(this).attr("data-target");
        var targetObject = $(wrapper).find('.tab-target[data-target="' + target + '"]');
        // classes
        $(wrapper).find(".is-active").removeClass("is-active");
        $(targetObject).addClass("is-active");
        $(this).addClass("is-active");
    });

});

// menu
jQuery(document).ready(function($) {

    $(".has-children").click(function(e) {
        e.preventDefault();
        $(".has-children").not(this).removeClass("active");
        $(this).toggleClass("active");
    });
    // this should stop the submenu links from not firing
    $(".sub-menu a").click(function(e) {
        e.stopPropagation();
    });

    // check if we're on any active ones
    var pathName = window.location.pathname;
    $("#navigation-sidebar a[href='" + pathName + "']").addClass("is-active");
    $("#navigation-sidebar a[href='" + pathName + "']").parents(".menu-item > div").addClass("is-active");

});

/********************************************************************
* Form validation
* ******************************************************************/

function validate(selector) {
    // loop through each of the possible inputs
    jQuery(selector).find(".required").each(function() {
        jQuery(this).removeClass("not-validated");
        if (jQuery(this).val() == "" || jQuery(this).val() == undefined || jQuery(this).val().lengh == 0) {
            jQuery(this).addClass("not-validated");
        }
    });
    // check if there's any not validated
    if (jQuery(selector + " .not-validated").length > 0) {
        return false;
    }
    return true;
}


/********************************************************************
* Form Dependencies
* ******************************************************************/

jQuery(document).ready(function($) {

    $(document).on("change, input", ".has-dependents", function() {
        // get base values
        var value = $(this).val();
        if ($(this).attr("type") == "radio") {
            var radioName = $(this).attr("data-dependency-key");
            value = $("[data-dependency-key='" + radioName + "']:checked").val();
        }
        var name = $(this).attr("name");
        // we can ovewrite name if we want to
        var dependentKey = $(this).attr("data-dependency-key");
        if (typeof dependentKey !== typeof undefined && dependentKey !== false) {
            name = dependentKey;
        }

        // clear out old dependent information
        // you could enter stuff in form elements and then switch
        // make sure this is on dependent-parent so it limits scope
        $(this).parents(".dependent-parent").find('.is-dependent[data-dependent-on="' + name + '"]').find("input,textarea").not("[type='radio']").val("");
        $(this).parents(".dependent-parent").find('.is-dependent[data-dependent-on="' + name + '"]').find("option:selected").prop("selected", false);

        // visibility
        $(this).parents(".dependent-parent").find('.is-dependent[data-dependent-on="' + name + '"].is-active').removeClass("is-active");
        $(this).parents(".dependent-parent").find('.is-dependent[data-dependent-on="' + name + '"][data-dependent-on-value="' + value + '"]').addClass("is-active");
        // I realize once again that my names are getting a little long winded here
        // but hey, at least you know what it's doing by reading it
        // I'm also assuming that if I used some fancy ass JS framework to do this
        // that a lot of this stuff I'm writing would already exist
        // but that very much limits the amount of people that can work on this
    });
});


/**
 * Running basic stuff on data actions
 * this allows us to quickly write out button commands
 */
jQuery(document).ready(function($) {

    $(document).on("click", "[data-action]", function(e) {
        // stop it from doing whatever
        e.preventDefault();

        // check if this is a local action
        var action = $(this).attr("data-action");
        if (typeof openStills[action] == "function") {
            openStills[action]($(this));
            return;
        }

        // put loading on it
        $(this).addClass("is-loading");
        var instance = $(this);

        // get all data
        var excludes = Array("data-action", "data-callback");
        var apiUrl = "/wp-json/openstills/" + $(this).attr("data-action");
        var senddata = {};
        $(this.attributes).each(function() {
            // don't include stuff we don't need
            if (excludes.indexOf(this.nodeName) > -1) {
                return true;
            }
            // only add data- attributes
            if (this.nodeName.indexOf("data-") === 0) {
                var newKey = this.nodeName.replace("data-", "");
                senddata[newKey] = this.nodeValue;
            }
        });

        // if this is a form include all inputs
        if ($(this).parents("form").length) {
            // NOTE: will probably need to do something for selects, non straight inputs
            // but right now this is only being used in one place
            $(this).parents("form").find("input").each(function() {
                senddata[$(this).attr("name")] = $(this).val();
            });
        }

        // test for any callback
        var callback = $(this).attr("data-callback");
        if (typeof callback == typeof undefined || callback == false) {
            callback = false;
        }

        // set up instance for callback
        var callbackId = createId();
        $(this).attr("data-callback-id", callbackId);

        // build query
        $.post({
            beforeSend: (xhr) => {
                // set the nonce so we can validate
                xhr.setRequestHeader('X-WP-Nonce', nonceData.nonce);
            },
            url: apiUrl,
            type: 'POST',
            data: senddata,
            success: (response) => {
                console.log(response);
                if (callback) {
                    openStills[callback](instance, response);
                }
                $(instance).removeClass("is-loading");
                $(instance).removeAttr("data-callback-id");
            },
            error: (response) => {
                console.log(response);
            }
        });
    });

});

/********************************************************************
* Special form element controls
* ******************************************************************/

// post list
// you can either search
// or click for a list of the latest of the post type
jQuery(document).ready(function($) {

    /**
     * Create the html for post lists
     * this is used a couple times so it made sense to put it in a function
     * @param  {object} value post information returened from the API
     * @return {string}       html output of the post list item
     */
    function getPostListHtml(value) {
        var html = '<a class="post-list-item panel-block" data-id="' + value.id + '" data-title="' + value.title.rendered + '">';
        html += '<div class="level">';
        html += '<div class="level-left">';
        html += '<span class="subtitle has-text-weight-bold is-size-6">' + value.title.rendered + '</span>';
        html += '</div>';
        html += '<div class="level-right">';
        html += '<i class="fa fa-plus has-text-primary add-object-meta-item"></i>';
        html += '</div>';
        html += '</div>';
        html += '</a>';
        return html;
    }

    /**
     * Display reccent most posts on the little icon click
     */
    $(".post-input-list-action").click(function() {
        var instance = $(this);
        // prepare query
        var postType = $(this).parents(".input-wrapper").attr("data-post-type");
        var apiUrl = "/wp-json/wp/v2/" + postType;

        // loading classes
        $(this).parents(".control").addClass("is-loading");

        // send query
        $.get({
            url: apiUrl,
            success: (response) => {
                console.log(response);

                // build html
                $.each(response, function(key, value) {
                    var html = getPostListHtml(value);
                    $(instance).parents(".input-wrapper").find(".post-list").append(html);
                });
            },
            error: (response) => {
                console.log(response);
                // create messages html
                addGenericError();
            }
        }).done(function() {
            $(instance).parents(".control").removeClass("is-loading");
        });
    });

    /**
     * Display posts on search
     */
    var timeoutId = 0;
    $(".post-search-input").on("input", function() {
        // clear out any previous posts
        $(this).parents(".input-wrapper").find(".post-list").html("");
        // get instance of object so we can get values and such
        var instance = $(this);
        // clear out any existing typing timer
        clearTimeout(timeoutId);
        // set new timeout
        timeoutId = setTimeout(function() {
            var searchVal = $(instance).val();
            // only search if there's more than 3 letters
            if (searchVal.length < 3) {
                return false;
            }

            // loading class
            $(instance).parents(".control").addClass("is-loading");

            // prepare query
            var postType = $(instance).parents(".input-wrapper").attr("data-post-type");
            var apiUrl = "/wp-json/wp/v2/" + postType;
            // send query
            $.get({
                url: apiUrl,
                data: {"search": searchVal},
                success: (response) => {
                    console.log(response);

                    // build html
                    $.each(response, function(key, value) {
                        var html = getPostListHtml(value);
                        $(instance).parents(".input-wrapper").find(".post-list").append(html);
                    });
                },
                error: (response) => {
                    console.log(response);
                    // create messages html
                    addGenericError();
                }
            }).done(function() {
                $(instance).parents(".control").removeClass("is-loading");
            });
        }, 1500);
    });

    /**
     * Click event for adding posts
     * I got a little crazy on long class names about right here
     * I should change that
     */
    $(document).on("click", ".post-input-wrapper .post-list-item", function() {
        // set up existing posts variable
        var existingPosts = $(this).parents(".post-input-wrapper").find(".post-input-hidden-input").val();
        // check if it's empty
        if (existingPosts == "" || typeof existingPosts == undefined) {
            existingPosts = "{}";
        }
        var parsedExistingPosts = JSON.parse(existingPosts);

        // get new post id and add it to the list
        // var newPost = $(this).attr("data-id");
        // parsedExistingPosts.push(newPost);
        parsedExistingPosts[$(this).attr("data-id").toString()] = {};

        // put it in to the input
        $(this).parents(".post-input-wrapper").find(".post-input-hidden-input").val(JSON.stringify(parsedExistingPosts));

    });

    /**
     * Adding extra field values to meta
     * put it in associative array in the hidden input
     */
    $(document).on("input", ".extra-field-input", function() {

        // get all post items that have been added
        var posts = $(this).parents(".selected-post-list").children("li");

        // create empty array
        var metaJSON = {};
        $(posts).each(function() {
            // key for main array (post id)
            var postKey = $(this).attr("data-id").toString();
            metaJSON[postKey] = {};
            // add all the extra meta
            $(this).find(".extra-field-input").each(function() {
                var key = $(this).attr("data-key");
                var value = $(this).val();
                metaJSON[postKey][key] = value;
            });
        });

        // put in to input
        $(this).parents(".field").find(".post-input-hidden-input").val(JSON.stringify(metaJSON));

    });



    /**
     * remove a post type from the added list
     * this is another place I went a little long on the class names
     */
    $(document).on("click", ".selected-post-list .delete-post-list-item", function() {
        var parentLi = $(this).parents("li");
        var metaJSON = {};
        $(this).parents("ul").find("li").not(parentLi).each(function() {
            var postKey = $(this).attr("data-id").toString();
            metaJSON[postKey] = {};
            // add all the extra meta
            $(this).find(".extra-field-input").each(function() {
                var key = $(this).attr("data-key");
                var value = $(this).val();
                metaJSON[postKey][key] = value;
            });
        });
        // add back in to value;
        $(this).parents(".field").find(".post-input-hidden-input").val(JSON.stringify(metaJSON));
        // remove this li
        $(parentLi).remove();
    });


    /**
     * Any time post-input-hidden-input changes we should populate the list of posts
     * this fires both whenever the page loads and whenever people add something to the list
     */
    $(".post-input-hidden-input").on("change", function() {

        // dont waste time on an empty value
        if ($(this).val().length == 0 || $(this).val() == "") {
            return false;
        }

        // get new list
        var postList = JSON.parse($(this).val());
        var instance = $(this);
        var extraFields = $(this).parents(".post-input-wrapper").find(".extra-field");

        $.each(postList, function(key, value) {
            // if it's already in the list we don't need to request it again
            // need to do the double quotes on this one because json
            if ($(instance).parents(".field").find(".selected-post-list [data-id='" + key + "']").length != 0) {
                return true;
            }
            // prepare query
            var postType = $(instance).parents(".input-wrapper").attr("data-post-type");
            var apiUrl = "/wp-json/wp/v2/" + postType + "/" + key;
            // send query
            $.get({
                url: apiUrl,
                success: (response) => {
                    console.log(response);

                    // build html
                    var html = '<li data-id="' + response.id + '">';
                    html += '<div class="has-text-weight-bold">' + response.title.rendered + '</div>';

                    $(extraFields).each(function() {
                        // keys and values
                        var fieldKey = $(this).attr("data-slug");
                        var fieldValue = value[fieldKey];
                        if (typeof fieldValue == "undefined") {
                            fieldValue = "";
                        }

                        // field html
                        html += '<div class="extra-field-input-wrapper">';
                        html += '<label class="label is-size-6">' + $(this).attr("data-label") + '</label>';
                        html += '<input type="text" class="extra-field-input input is-small" data-key="' + fieldKey + '" value="' + fieldValue + '" />';
                        html += '</div>';
                    });

                    html += '<div class="actions">';
                    html += '<a class="delete-post-list-item has-text-danger"><i class="fa fa-times-circle"></i></a>';
                    html += '</div>';
                    html += '</li>';

                    // append html
                    $(instance).parents(".field").find(".selected-post-list").append(html);

                },
                error: (response) => {
                    console.log(response);
                    // create messages html
                    addGenericError();
                }
            });
        });
    });

    /**
     * Radio buttons
     * in certain forms we're using data-name instead of name so it doesn't register
     * this means that by default radio buttons wont unclick
     */
    $('input[type="radio"][data-name]').click(function() {
        var dataName = $(this).attr("data-name");
        $('input[type="radio"][data-name="' + dataName + '"]').not(this).prop("checked", false);
    });
    $('input[type="radio"][data-dependency-key]').click(function() {
        var dataName = $(this).attr("data-dependency-key");
        $('input[type="radio"][data-dependency-key="' + dataName + '"]').not(this).prop("checked", false);
    });

});

/********************************************************************
* Messages
* ******************************************************************/

function addGenericError(message = false) {
    var html = '<article class="message is-danger error-message">';
    html += '<div class="message-body">';
    if (message) {
        html += message;
    } else {
        html += "Oh no! Something went wrong. If you know what you're doing, look at the console for more info";
    }
    html += '</div>';
    html += '</article>';
    jQuery(".messages").append(html);

    // remove error message
    setTimeout(function() {
        jQuery(".error-message").slideUp(function() {
            jQuery(this).remove();
        });
    }, 10000);
}

function addSuccess(message = "Success!") {
    var html = '<article class="message is-success success-message">';
    html += '<div class="message-body">';
    html += message
    html += '</div>';
    html += '</article>';
    jQuery(".messages").append(html);

    // remove error message
    setTimeout(function() {
        jQuery(".success-message").slideUp(function() {
            jQuery(this).remove();
        });
    }, 10000);
}



/********************************************************************
* Helpers
* ******************************************************************/

/**
 * Get the value of a query var
 * @param  {string} name the query var
 * @return {string|boolean}          value of query var on success, false on failure
 */
function getQueryVar(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

/**
 * Pad numbers
 * @param  {integer} num  the number to get padded
 * @param  {integer} size how many total digits
 * @return {string}      padded number
 */
function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

/**
 * Watching for change on hidden inputs
 * This is mostly used for post type inputs
 */
jQuery(document).ready(function($) {

    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

    var trackChange = function(elements) {

        $.each(elements, function(key, element) {
            var observer = new MutationObserver(function(mutations, observer) {
                if(mutations[0].attributeName == "value") {
                    $(element).trigger("change");
                }
            });
            observer.observe(element, {
                attributes: true
            });
        });
    }

    // Just pass an element to the function to start tracking
    trackChange($('input[type="hidden"]'));
});


var unserialize = function(object) {
    var returnArray = {};
    // loop through each key
    jQuery.each(object, function(key, value) {
        var propArray = {};
        if (typeof value == "object") {
            childArray = unserialize(value);
            jQuery.each(childArray, function(childKey, childValue) {
                propArray["[" + key + "]" + childKey] = childValue;
            });
        } else {
            var propKey = "[" + key + "]";
            if (typeof value == "boolean") {
                propKey += ":boolean";
            }
            propArray[propKey] = value;
        }
        returnArray = jQuery.extend(returnArray, propArray);
    });
    return returnArray;
}


/**
 * Create a random ID
 * @param  {int} length how long should this ID be, defaults to 10
 * @return {string}        random ID
 */
function createId(length = 10) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

/**
 * Toggle inputs
 */

jQuery(document).ready(function($) {

    $(document).on("click", ".toggle", function() {
        // toggle classes
        $(this).toggleClass("is-false");
        $(this).toggleClass("is-true");

        $(this).addClass("jambalaya");
        // set input value
        if ($(this).hasClass("is-true")) {
            $(this).find("input[type='hidden']").val("true");
        } else if ($(this).hasClass("is-false")) {
            $(this).find("input[type='hidden']").val("false");
        }
    });

});


/********************************************************************
* Graphs
* ******************************************************************/


jQuery(document).ready(function($) {

    // graph colors
    var graphColors = {
        "Mash Temp": Array("rgba(255, 56, 96, 0.5)", "rgba(255, 56, 96, 0.1)"),
        "Column Temp": Array("rgba(255, 56, 96, 0.5)", "rgba(255, 56, 96, 0.1)"),
        "Distillate Temp": Array("rgba(50, 115, 220, 0.5)", "rgba(50, 115, 220, 0.1)"),
        "ABV": Array("rgba(35, 209, 96, 0.5)", "rgba(35, 209, 96, 0.1)"),
        "Volume": Array("rgba(32, 156, 238, 0.5)", "rgba(32, 156, 238, 0.1)"),
        "Temperature": Array("rgba(255, 56, 96, 0.5)", "rgba(255, 56, 96, 0.1)"),
        "Specific Gravity": Array("rgba(1, 209, 178, 0.5)", "rgba(1, 209, 178, 0.1)"),
        "Ph": Array("rgba(25, 209, 96, 0.5)", "rgba(35, 209, 96, 0.1)")
    };

    /**
     * Any object with a class of graph gets made in to a graph
     */
    var graphList = {};
    $(".graph").each(function() {

        // Build sensor data
        var dataSetsArray = Array();
        var sensors = $(this).siblings(".sensor-data");
        $(sensors).each(function() {
            var dataLabel = $(this).attr("data-label");
            var pushObj = {
                sensorId: $(this).attr("data-id"),
                label: $(this).attr("data-label"),
                backgroundColor: graphColors[dataLabel][1],
                borderColor: graphColors[dataLabel][0],
                data: [],
                showLine: true,
                fill: false
            }
            dataSetsArray.push(pushObj);
        });


        // build time data
        // get non-JS values of this (unix time)
        var startTime = $(this).attr("data-start-time");
        if (typeof startTime == typeof undefined || startTime == false) {
            startTime = Math.floor(Date.now() / 1000);
        }
        startTime = parseInt(startTime);
        var timeSpan = parseInt($(this).attr("data-time-span"));
        var endTime = startTime + (60 * 60 * timeSpan);

        // convert to javascript time
        startTime = new Date(startTime * 1000);
        endTime = new Date(endTime * 1000);
       

       // get other chart data
        var chartType = $(this).attr("data-type");

        var chartMin = $(this).attr("data-min");
        var chartMax = $(this).attr("data-max");


        // start chart
        var newChart = new Chart($(this), {
            type: chartType,
            data: {
                datasets: dataSetsArray
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            min: parseInt(chartMin),
                            max: parseInt(chartMax),
                            stepSize: 10
                        }
                    }],
                    xAxes: [{
                        type: "time",
                        time: {
                            min: startTime,
                            max: endTime
                        }
                    }]
                }
            }
        });

        var chartId = $(this).attr("id");
        // populate data in chart
        $(sensors).each(function() {

            var sensorId = $(this).attr("data-id");
            var sensorLabel = $(this).attr("data-label");
            var postId = $(this).parents("article").attr("data-post-id");
            var apiUrl = "/wp-json/openstills/sensor/getdata";

            $.get({
                url: apiUrl,
                type: 'GET',
                data: {"sensor-id": sensorId, "post-id": postId},
                success: (response) => {
                    if (response){
                        // There must be a better way to do this
                        // not sure how well it's going to fare with large data sets
                        $.each(response, function(time, value) {
                            // format for JS
                            var timeStamp = new Date(time * 1000);
                            var pushData = {x: timeStamp, y: value};

                            // find the correct dataset to add to
                            $.each(newChart.data.datasets, function(key, value) {
                                if (value.sensorId == sensorId) {
                                    newChart.data.datasets[key].data.push(pushData);
                                    return false;
                                }
                            });
                        });

                        //trigger update
                        newChart.update();

                        // add to chart array
                        graphList[chartId] = newChart;
                    }
                },
                error: (response) => {
                }
            });
        });


    });

    // update those graphs

});



/********************************************************************
* Timers
* These run on any timer on the site
* has to be formatted properly in the DOM
* ******************************************************************/

jQuery(document).ready(function($) {

    /**
     * Time updating
     */

    openStills.timerIsRunning = true;

    openStills.runTimeUpdate = function() {

        // set that this is running
        openStills.timerIsRunning = true;

        $(".time-stat").each(function() {

            // check if it has started
            var startTime = $(this).attr("data-start-time");
            if (startTime == "" || typeof startTime == typeof undefined) {
                return true;
            }

            // check if currently active
            var endTime = $(this).attr("data-stop-time");

            var isActive = false;
            if (endTime == "" || typeof endTime == typeof undefined) {
                endTime = Math.round((Date.now() / 1000));
                isActive = true;
            }

            // if we only want to update this once
            // take away the class that updates this
            if (!isActive) {
                $(this).removeClass("time-stat");
            }

            // set time to now
            var now = new Date();
            var offset = now.getTimezoneOffset() + 60;
            // calculate time
            // get total seconds between the times
            var delta = Math.abs(startTime - endTime);
            // calculate (and subtract) whole days
            var days = Math.floor(delta / 86400);
            delta -= days * 86400;
            // calculate (and subtract) whole hours
            var hours = Math.floor(delta / 3600) % 24;
            delta -= hours * 3600;
            // calculate (and subtract) whole minutes
            var minutes = Math.floor(delta / 60) % 60;
            delta -= minutes * 60;
            // what's left is seconds
            var seconds = Math.round(delta % 60);

            // format the time
            var timeString = $(this).attr("data-format");
            timeString = timeString.replace("DD", pad(days, 2));
            timeString = timeString.replace("HH", pad(hours, 2));
            timeString = timeString.replace("MM", pad(minutes, 2));
            timeString = timeString.replace("SS", pad(seconds, 2));

            // update info
            $(this).find(".value").text(timeString);

        });

        // check if we should run this again
        if ($(".time-stat.active").length > 0) {
            // run again
            setTimeout(function() {
                openStills.runTimeUpdate();
            }, 1000);
        } else {
            openStills.timerIsRunning = false;
        }

    }
    // run first time
    openStills.runTimeUpdate();

});


/********************************************************************
* Sensor sliders
* ******************************************************************/

jQuery(document).ready(function($) {

    $(".stats-slider").each(function() {
        $(this).slick({
            slidesToShow: 4,
            dots: false,
            arrows: false,
            slide: '.stat'
        });
    });

    // value inputs
    $(".manual-input").click(function() {
        $(this).parents(".stat").toggleClass("manual-active");
    });

    // callback to hide the input
    openStills.manualSensorInputCallback = function(instance) {
        $(instance).parents("form").find("[name='data']").val("");
        $(instance).parents(".stat").removeClass("manual-active");
    };

});

