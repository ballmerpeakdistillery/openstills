/********************************************************************
* Calendar
********************************************************************/

jQuery(document).ready(function($) {

    var months = Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

    $(".calendar").each(function() {

        var calendar = $(this);

        // load base html
        var headingHtml = '<div class="calendar-header">';
        headingHtml += '<span class="prev-month"><i class="fa fa-caret-left"></i></span>';
        headingHtml += '<span class="month-label"></span>';
        headingHtml += '<span class="next-month"><i class="fa fa-caret-right"></i></span>';
        headingHtml += '</div>';
        $(this).append(headingHtml);

        // insert days wrapper
        var daysWrapper = '<div class="calendar-content"></div>';
        $(this).append(daysWrapper);

        function loadMonth(month, year) {
            // put in label
            $(calendar).find(".month-label").text(months[month] + " " + year);

            // put in empty days
            $(calendar).find(".calendar-content").html("");
            var html = "";
            for (var i = 0; i < 7; i++) {
                html += '<div class="week columns">';
                for (var o = 0; o < 7; o++) {
                    html += '<div class="day column"></div>';
                }
                html += '</div>';
            }
            $(calendar).find(".calendar-content").html(html);

            // put in day labels
            // get month info
            var targetMonth = new Date();
            targetMonth.setFullYear(year, month, 1);
            var startDay = targetMonth.getDay();
            var daysInMonth = new Date(year, month, 0).getDate();

            var daysDomObjects = $(calendar).find(".day");
            $(daysDomObjects).addClass("not-in-month");

            var domSliceStart = startDay;
            if (startDay == 0) {
                domSliceStart = 6;
            }
            var monthDays = $(daysDomObjects).slice(domSliceStart, startDay + daysInMonth);

            $(monthDays).removeClass("not-in-month").addClass("in-month");

            $(monthDays).each(function(key) {
                $(this).attr("data-day", key + 1);
                $(this).attr("data-full-date", (month + 1) + "-" + (key + 1) + "-" + year);
            });

            // hide any weeks that have no days in month
            $(calendar).find(".week").each(function() {
                if ($(this).find(".not-in-month").length == 7) {
                    $(this).remove();
                }
            });

            // put a selected class on specific day
            var selectedDay = $(calendar).attr("data-selected-day");
            $('.day[data-full-date="' + selectedDay + '"]').addClass("selected");
            
        }

        // load initial month
        var currentMonth = $(this).attr("data-month");
        var currentYear = $(this).attr("data-year");

        loadMonth(currentMonth, currentYear);

        // navigation
        $(calendar).on("click", ".prev-month", function() {
            var currentMonth = parseInt($(calendar).attr("data-month"));
            var currentYear = parseInt($(calendar).attr("data-year"));
            if (currentMonth == 0) {
                var newMonth = 11;
                var newYear = currentYear - 1;
            } else {
                var newMonth = currentMonth - 1;
                var newYear = currentYear;
            }
            $(calendar).attr("data-month", newMonth);
            $(calendar).attr("data-year", newYear);

            loadMonth(newMonth, newYear);
        });
        $(calendar).on("click", ".next-month", function() {
            var currentMonth = parseInt($(calendar).attr("data-month"));
            var currentYear = parseInt($(calendar).attr("data-year"));
            if (currentMonth == 11) {
                var newMonth = 0;
                var newYear = currentYear + 1;
            } else {
                var newMonth = currentMonth + 1;
                var newYear = currentYear;
            }
            $(calendar).attr("data-month", newMonth);
            $(calendar).attr("data-year", newYear);

            loadMonth(newMonth, newYear);
        });

    });


    /*

    // get current month
    var dateObj = new Date();
    var month = dateObj.getMonth();
    var year = dateObj.getFullYear();

    var selectedMonth = month;

    loadMonth(month, year);

    function loadMonth(month, year) {
        // put in label
        $("#month-label").text(months[month] + " " + year);
        // clear out data
        $(".day").removeClass("not-in-month").removeClass("in-month").attr("data-day", "");
        $(".day").html("");
        // set month on calendar. This is to avoid any lag in getting the posts for that month
        $("#calendar").attr("data-month", month);

        // get month info
        var targetMonth = new Date();
        targetMonth.setFullYear(year, month, 1);
        var startDay = targetMonth.getDay();
        var daysInMonth = new Date(year, month, 0).getDate();

        var daysDomObjects = $("#calendar").find(".day");
        $(daysDomObjects).addClass("not-in-month");

        var domSliceStart = startDay - 1;
        if (startDay == 0) {
            domSliceStart = 6;
        }
        var monthDays = $(daysDomObjects).slice(domSliceStart, startDay + daysInMonth - 1);

        $(monthDays).removeClass("not-in-month").addClass("in-month");

        $(monthDays).each(function(key) {
            $(this).attr("data-day", key + 1);
        });

        // get all events for that date
        var apiUrl = "/wp-json/digitaloperative/getevents/?month=" + (month + 1) + "&year=" + year;
        var sendData = {
            "month": month,
            "year": year
        };
        $.get(apiUrl, function(data) {
            // if there's any lag, don't load the events
            if (month != $("#calendar").attr("data-month")) {
                return false;
            }
            $.each(data, function(key, value) {
                var date = value.date;
                // insert blank list if it doesn't have one already
                if ($('.day[data-day="' + date + '"]').find("ul").length == 0) {
                    $('.day[data-day="' + date + '"]').append("<ul></ul>");
                }
                var html = '<li data-type="' + value.type + '">' + value.title + '</li>';
                $('.day[data-day="' + date + '"]').find("ul").append(html);
            });
        });
    }

    $('[data-action="next"]').click(function() {
        month++;
        if (month == 12) {
            month = 0;
            year++;
        }
        loadMonth(month, year);
    });
    $('[data-action="prev"]').click(function() {
        month--;
        if (month == 0) {
            month = 11;
            year--;
        }
        loadMonth(month, year);
    });

    */

});