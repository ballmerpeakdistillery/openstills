/********************************************************************
* Comments scripts
********************************************************************/

jQuery(document).ready(function($) {


    /**
     * Unhide the form
     */
    openStills.unhideCommentForm = function(instance) {
        $("#comment-form-wrapper").removeClass("hidden");
        $(instance).addClass("hidden");
    }

    /**
     * posts comment info to API endpoint
     * @param  {object} instance DOM object of the button that got pressed
     */
    openStills.saveComment = function(instance) {

        // styling
        $(instance).addClass("is-loading");

        // get data
        var senddata = $(instance).parents("form").serializeJSON();
        var apiUrl = $(instance).parents("form").attr("action");

        // handle the API call
        $.post({
            beforeSend: (xhr) => {
                // set the nonce so we can validate
                xhr.setRequestHeader('X-WP-Nonce', nonceData.nonce);
            },
            url: apiUrl,
            type: 'POST',
            data: senddata,
            success: (response) => {
                console.log(response);
                // take off loading
                $(instance).removeClass("is-loading");

                // add the html
                html = '<li class="comment-list-item">';
                html += '<div class="comment-meta">';
                html += '<span class="user has-text-weight-bold has-text-info">Me</span>';
                html += '<span class="time has-text-grey-light">Now</span>';
                html += '</div>';
                html += '<div class="comment-content has-text-grey-dark">';
                html += senddata.content;
                html += '</div>';
                html += '</li>';

                // add to list
                $("#comment-list").prepend(html);

                // clear out comment form
                $("#comment-form-wrapper").find("textarea").html("");

                // styles
                $("#comment-form-wrapper").addClass("hidden");
                $('[data-action="unhideCommentForm"]').removeClass("hidden");

            },
            error: (response) => {
                console.log(response);
                // create messages html
                addGenericError();
            }
        });

    }


});