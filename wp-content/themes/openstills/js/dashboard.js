/********************************************************************
* Dashboard
********************************************************************/

jQuery(document).ready(function($) {
    

    /**
     * Add item to the dashboard
     */
    openStills.addDashboardItem = function(instance) {
        
        $(instance).addClass("is-loading");

        // get all needed info
        var dashboardItem = $("#dashboard-select").val();
        var apiUrl = "/wp-json/openstills/dashboard/getitemhtml";
        var senddata = {"item": dashboardItem};

        // post to endpoint
        $.post({
            url: apiUrl,
            data: senddata,
            success: (response) => {
                var html = response.html;
                $("#dashboard-items").append(html);
            },
            error: (response) => {
                console.log(response);
                // create messages html
                addGenericError();
            }
        }).done(function() {
            $(instance).removeClass("is-loading");
        });
    }

    /**
     * Post the dashboard items to the API endpoint
     */
    openStills.saveDashboard = function(instance) {

        $(instance).addClass("is-loading");

        var dashboardItems = Array();
        $(".dashboard-item").each(function() {
            dashboardItems.push($(this).attr("data-id"));
        });

        console.log(dashboardItems);

        var apiUrl = "/wp-json/openstills/dashboard/savedashboard";

        $.post({
            beforeSend: (xhr) => {
                // set the nonce so we can validate
                xhr.setRequestHeader('X-WP-Nonce', nonceData.nonce);
            },
            url: apiUrl,
            data: {"dashboardItems": dashboardItems},
            success: (response) => {
                addSuccess("Dashboard Saved");
            },
            error: (response) => {
                console.log(response);
                // create messages html
                addGenericError();
            }
        }).done(function() {
            $(instance).removeClass("is-loading");
        });

    }

});