/********************************************************************
* Ferment scripts
* ******************************************************************/

jQuery(document).ready(function($) {

    openStills.stopFermentCallback = function(instance) {

        // styling
        $(instance).removeClass("is-danger").removeClass("is-loading").addClass("is-info");
        $(instance).text("New Stripping Run");
        $(instance).removeAttr("data-action");
        $(instance).attr("href", "/add/production/run/");

        // add end time
        $('[data-stat="ferment-run-time"]').attr("data-stop-time", Math.round(Date.now() / 1000));

        // unhide the thransfer stuff
        $(".transfer-wrapper").removeClass("hidden");
    }

});