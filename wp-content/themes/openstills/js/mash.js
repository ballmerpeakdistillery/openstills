/********************************************************************
* Mash scripts
* ******************************************************************/

jQuery(document).ready(function($) {


    /**
     * Start mash actions
     */
    openStills.startMashCallback = function(instance) {

        // switch out butotn stuff
        $(instance).removeClass("is-primary").addClass("is-danger");
        $(instance).text("End Mash");
        $(instance).attr("data-action", "mash/stopmash");
        $(instance).attr("data-callback", "stopMashCallback");

        // put in start time (might not 100% match server time)
        $('[data-stat="mash-run-time"]').attr("data-start-time", Math.round(Date.now() / 1000));
        $('[data-stat="mash-run-time"]').addClass("active").removeClass("new");

        // run timer if it's not already running somewhere
        if (!openStills.timerIsRunning) {
            openStills.runTimeUpdate();
        }
    }


    /**
     * End mash actions
     */
    openStills.stopMashCallback = function(instance) {

        // styling
        $(instance).removeClass("is-danger").removeClass("is-loading").addClass("is-info");
        $(instance).text("New Ferment");
        $(instance).removeAttr("data-action");
        $(instance).attr("href", "/add/production/ferment/");

        // add end time
        $('[data-stat="mash-run-time"]').attr("data-stop-time", Math.round(Date.now() / 1000));

    }


});