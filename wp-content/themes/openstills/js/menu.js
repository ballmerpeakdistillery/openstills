/********************************************************************
* Menu scripts
* ******************************************************************/

jQuery(document).ready(function($) {

    $("#defined-menu").sortable();
    $(".menu-group").sortable();

    $("#defined-menu").on('DOMNodeInserted', function(e) {
        $('[data-action="saveMenu"]').removeClass("disabled");
    });
    $("#defined-menu").on('DOMNodeRemoved', function(e) {
        $('[data-action="saveMenu"]').removeClass("disabled");
    });

    /****************************************************************
    * adding things to the menu
    * **************************************************************/

    $('[data-action="addMenuGroup"]').click(function() {

        $(".menu-group.active-menu-group").removeClass("active-menu-group");

        var html = '<div class="menu-group active-menu-group">';
        html += '<input type="text" class="input is-small group-name" placeholder="New Group" />';
        html += '</div>';

        $("#menu-items").append(html);

    });

    $("#menu-items").sortable();

    $(document).on("click", ".menu-group", function() {
        $(".menu-group.active-menu-group").removeClass("active-menu-group");
        $(this).addClass("active-menu-group");
    });

    $('[data-action="addMenuObject"]').click(function() {

        var namespace = $(this).attr("data-namespace");
        var object = $(this).attr("data-object");
        var rewrite = $(this).attr("data-rewrite");

        var html = '<div class="menu-item columns" data-namespace="' + namespace + '" data-object="' + object + '" data-type="object" data-rewrite="' + rewrite + '">';
        html += '<div class="column is-6">' + object + '</div>';

        html += '<div class="column is 5">';
        html += '<i class="fa fa-plus action enabled" data-action="toggleObjectLink" data-item="add"></i>';
        html += '<i class="fa fa-list action enabled" data-action="toggleObjectLink" data-item="archive"></i>';
        html += '<i class="fa fa-gears action enabled" data-action="toggleObjectLink" data-item="settings"></i>';
        html += '</div>'

        html += '<div class="column is-1">';
        html += '<i class="fa fa-trash action" data-action="deleteMenuItem"></i>'
        html += '</div>';

        html += '</div>';

        $(".active-menu-group").append(html);

        // make sure it's sortable
        $(".active-menu-group").sortable();
    });

    // turn off menu links
    // $(document).on("click", '[data-action="toggle-object-link"]', function() {
    openStills.toggleObjectLink = function(instance) {
        $(instance).toggleClass("enabled");
    }

    /****************************************************************
    * Removing things from the menu
    * **************************************************************/

    //$(document).on("click", '[data-action="delete-menu-item"]', function() {
    openStills.deleteMenuItem = function(instance) {
        $(instance).parents(".menu-item").remove();
    }

    /****************************************************************
    * Saving the menu
    * **************************************************************/

    //$('[data-action="save-menu"]').click(function() {
    openStills.saveMenu = function(instance) {
        $(instance).addClass("is-loading");

        // build json
        var menuItems = Array();
        $(".menu-group").each(function() {
            var groupName = $(this).find(".group-name").val();
            var groupArray = {
                "label": groupName,
                "items": Array()
            };
            // loop through items
            $(this).find(".menu-item").each(function() {
                var type = $(this).attr("data-type");
                switch (type) {
                    case "object":
                        var linkItems = Array();
                        $(this).find(".enabled").each(function() {
                            linkItems.push($(this).attr("data-item"));
                        });
                        var itemArray = {
                            "type": "object",
                            "namespace": $(this).attr("data-namespace"),
                            "object": $(this).attr("data-object"),
                            "links": linkItems,
                            "rewrite": $(this).attr("data-rewrite")
                        }
                        break;
                    case "link":

                        break;
                }
                groupArray["items"].push(itemArray);
            });
            menuItems.push(groupArray);
        });

        // submit
        var apiUrl = "/wp-json/openstills/savemenu";
        $.post({
            beforeSend: (xhr) => {
                // set the nonce so we can validate
                xhr.setRequestHeader('X-WP-Nonce', nonceData.nonce);
            },
            url: apiUrl,
            data: {"menu": menuItems},
            success: (response) => {
                addSuccess();
            },
            error: (response) => {
                console.log(response);
                // create messages html
                addGenericError();
            }
        }).done(function() {
            $('[data-action="save-menu"]').removeClass("is-loading");
        });
    };

});