/********************************************************************
* Notification scripts
********************************************************************/

jQuery(document).ready(function($) {

    /**
    * click management
     */
    $(document).on("click", ".notification-list-item", function(e) {
        e.stopPropagation();
    });

    /**
     * unhide the notification list
     */
    $("#notification-icon").click(function(e) {
        $(this).addClass("active");
    });

    /**
     * Load notification html in to the list
     * @param  {DOM object} instance the itme that has been clicked
     * @param  {json} response response from the api call
     */
    openStills.loadNotificationHtml = function(instance, response) {
        $("#notification-list").html(response["html"]);
    }


    /**
     * Remove notification once it's marked as read
     */
    openStills.removeNotification = function(instance, response) {
        // remove the items
        $(instance).parents(".notification-list-item").remove();
        // update the number
        var currentCount = parseInt($("#notification-icon").attr("data-count"));
        var newCount = currentCount - 1;
        $("#notification-icon").attr("data-count", newCount);
    }

    /*
    $(document).click(function() {
        if ($("#notification-icon").hasClass("active")) {
            $("#notification-list").html("");
            $("#notification-icon").removeClass("active");
        }
    });
    */

});