/********************************************************************
* Scripts related to Object functionality
* ******************************************************************/



/********************************************************************
* Save object
* ******************************************************************/

jQuery(document).ready(function($) {
    $("#edit-object form").on("submit", function(e) {
        var instance = $(this);
        // loading stuff
        $(this).find('button[type="submit"]').addClass("is-loading");
        // dont submit
        e.preventDefault();

        // we want to take any form values that don't apply out
        // this happens for 2 elements, validation and type because they have empty values
        // or "none"
        // so just take anything that's hidden, remove the name for now. We can rekey on success
        $(".object-meta-section").not(".is-active").find("[name]").removeAttr("name");

        var formValues = $("#object-edit-form").serializeJSON();
        var apiUrl = "/wp-json/openstills/saveobject";
        // send query
        $.post({
            url: apiUrl,
            data: {json:JSON.stringify(formValues)},
            success: (response) => {
                console.log(response);
                addSuccess();
                // set "redirect"
                var stateObj = {"state": "oftheunion"};
                var objectSlug = $("[name='config[rewrite][slug]']").val();
                var namespace = $('[name="namespace"]').val();
                var newPageTitle = "Edit - " + $("[name='config[labels][name]']").val();
                $("#page-title").text(newPageTitle)
                history.replaceState(stateObj, newPageTitle, "/settings/object/edit/" + objectSlug);

                // put names back on everything that we didn't do before
                reKey();
            },
            error: (response) => {
                console.log(response);
                // create messages html
                addGenericError();
            }
        }).done(function() {
            $(instance).find('button[type="submit"]').removeClass("is-loading");
        });
    });
});


/********************************************************************
* Load object
* ******************************************************************/

var initialPostObject;

jQuery(document).ready(function($) {

    // quit out of this bit if there's no object defined
    if (typeof object == typeof undefined || object == false) {
        return false;
    }

    // turn on loading class
    $("#edit-object").addClass("is-loading");

    // load object data
    var objectUrl = "/wp-content/themes/openstills/objects/" + object + "-config.json";
    $.get(objectUrl, function(data) {

        // load base config
        var configData = unserialize(data.config);
        initialPostObject = configData;

        $.each(configData, function(key, value) {
            var formKey = "config" + key;
            var formElement = $('[name="' + formKey + '"]');
            if ($(formElement).attr("type") == "radio") {
                // radio elements have a little extra goign on
                // not just clicking a value
                $('[name="' + formKey + '"]').prop("checked", false);
                $('[name="' + formKey + '"][value="' + value.toString() + '"]').prop("checked", true);
            } else {
                $(formElement).val(value);
            }
        });

        // load namespace
        $('[name="namespace"]').val(data.namespace);

        var unserializedData = unserialize(data.meta);
        $.each(data.meta, function(key, value) {
            // click new meta and add in title
            $("#new-meta-group").click();
            $(".current-meta-group").find(".meta-group-name").val(key);
            $.each(value, function(metaKey, metaConfig) {
                console.log(metaConfig);
                // click new meta
                $(".current-meta-group").find(".new-meta-field").click();

                for (var i = 0; i < 3; i++) {
                    $(".current-meta-item").find("[data-fill-order='" + i + "']").each(function() {
                        // this name reflects whats in the unserialized array
                        var name = $(this).attr("name").replace("meta", "");
                        if (name in unserializedData) {
                            $(this).val(unserializedData[name]);
                            $(this).trigger("input");
                        }
                    });
                }

                // select options
                if ("options" in metaConfig) {
                    $.each(metaConfig["options"], function(optionKey, optionValue) {
                        $(".current-meta-item").find(".new-option-label").val(optionValue["label"]);
                        $(".current-meta-item").find(".new-option-value").val(optionValue["value"]);
                        $(".current-meta-item").find('[data-action="addSelectOption"]').click();
                    });
                }

                // extra object fields
                if ("extra-fields" in metaConfig) {
                    $.each(metaConfig["extra-fields"], function(fieldKey, fieldValue) {
                        $(".current-meta-item").find('[data-action="addExtraField"]').click();
                        var label = fieldValue["label"];
                        $(".current-meta-item").find(".extra-field-name").last().val(label);
                        var slug = fieldValue["slug"];
                        $(".current-meta-item").find(".extra-field-slug").last().val(slug);
                    });
                }

                // required stuff
                // this can come last as it doesn't have dependencies
                if (metaConfig["required"] == "true") {
                    $(".current-meta-item").find("[data-name='meta[metaGroup][metaKey][required]']").parent(".toggle").click();
                }

                // finally, do the size
                var size = parseInt(metaConfig["meta-size"].replace("is-", ""));
                var clicks = 12 - size;
                for (var i = 0; i < clicks; i++) {
                    $(".decrement-size").click();
                }
            });
        });

        // load taxonomies
        if ("taxonomy" in data) {
            $.each(data["taxonomy"], function(key, taxonomyData) {
                $("#new-taxonomy").click();
                $('[data-name="taxonomy[taxonomyKey][label]"]').val(taxonomyData["label"]);
                $('[data-name="taxonomy[taxonomyKey][slug]"]').val(taxonomyData["slug"]);
                $('[data-action="addTaxonomy"]').click();
            });
        }

        $("#edit-object").removeClass("is-loading");
    });

});


/********************************************************************
* Input specific functions
* ******************************************************************/

jQuery(document).ready(function($) {

    // unfortunately need a separate one for on input and on change
    $('[name="config[labels][singular_name]"]').on("input", function() {
        // create a default slug
        var namespace = $('[name="namespace"]').val();
        var parsedSlug = $('[name="config[labels][singular_name]"]').val().toLowerCase().replace(" ", "-");

        // place the default slug in
        $('[name="config[rewrite][slug]"]').val(namespace + "/" + parsedSlug);
    });
    $('[name="namespace"]').on("change", function() {
        // create a default slug
        var namespace = $('[name="namespace"]').val();
        var parsedSlug = $('[name="config[labels][singular_name]"]').val().toLowerCase().replace(" ", "-");

        // place the default slug in
        $('[name="config[rewrite][slug]"]').val(namespace + "/" + parsedSlug);
    });

});


/********************************************************************
* Meta functions
* ******************************************************************/

var reKey;

jQuery(document).ready(function($) {

    /****************************************************************
    * Meta groups
    * **************************************************************/

    $("#new-meta-group").click(function() {

        // get base info
        var groupCount = $(".meta-group").length + 1;
        $(".current-meta-group").removeClass("current-meta-group");

        // build html
        var html = '<div class="meta-group current-meta-group">';

        html += '<div class="group-properties">';
        html += '<input type="text" class="meta-group-name" value="New Meta Group ' + groupCount + '" />';
        html += '<div class="is-pulled-right">';
        html += '<i class="fa fa-trash delete-meta-group"></i>';
        html += '</div>';
        html += '</div>';

        html += '<div class="meta-list columns is-multiline"></div>';

        html += '<span class="button is-primary new-meta-field">';
        html += '<span class="icon"><i class="fa fa-plus"></i></span>';
        html += '<span>New Meta Field</span>';
        html += '</span>';

        html += '</div>';

        // add html
        $("#meta-group-list").append(html);
    });




    $("#meta-field-list").sortable({
        stop: function(event, ui) {
            // rekey everything
            var optionsRegex = /meta\[.\]/;
            // loop through all list items that we have
            $(".meta-list-item").each(function(key) {
                $(this).attr("data-meta-key", key);
                // loop through all inputs within that list item
                $(this).find("[name]").each(function() {
                    var name = $(this).attr("name");
                    var newName = name.replace(optionsRegex, "meta[" + key + "]");
                    $(this).attr("name", newName);
                });
            });
        }
    });


    /**
     * Creating slugs from the name input
     */
    $(document).on("input", ".meta-name", function() {
        var metaName = $(this).val();
        var metaSlug = metaName.replace(/ /g, '-').toLowerCase();
        $(this).next(".meta-slug").val(metaSlug);
    });

    /**
     * Change the size of the meta
     */
    $(document).on("click", ".increment-size", function() {
        var size = $(".current-meta-item").attr("data-span-size");
        size = parseInt(size);

        // don't do anything over 12
        if (size == 12) {
            return false;
        }

        // new size
        var newSize = size + 1;
        $(".current-meta-item").attr("data-span-size", newSize);

        // classes
        $(".current-meta-item").removeClass("is-" + size);
        $(".current-meta-item").addClass("is-" + newSize);

        // add to input
        $(".current-meta-item").find(".meta-size").val("is-" + newSize);
    });
    $(document).on("click", ".decrement-size", function() {
        var size = $(".current-meta-item").attr("data-span-size");
        size = parseInt(size);

        // don't do anything over 12
        if (size == 3) {
            return false;
        }

        // new size
        var newSize = size - 1;
        $(".current-meta-item").attr("data-span-size", newSize);

        // classes
        $(".current-meta-item").removeClass("is-" + size);
        $(".current-meta-item").addClass("is-" + newSize);

        // add to input
        $(".current-meta-item").find(".meta-size").val("is-" + newSize);
    });

    /**
     * Deleting meta items
     */
    
    $(".delete-meta-field").click(function() {
        $(".current-meta-item").remove();
        reKey();
    });

    /**
     * making sure that there's a selected meta for meta actions
     */
    $(".meta-list-item").click(function() {
        $(".current-meta-item").removeClass("current-meta-item");
        $(this).addClass("current-meta-item");

        $("#current-meta-actions").removeClass("disabled");
    });

    /****************************************************************
    * Adding new object meta
    * **************************************************************/


    $(document).on("click", ".new-meta-field", function() {
        // make sure we know what group this is for
        $(".current-meta-group").removeClass("current-meta-group");
        $(this).parents(".meta-group").addClass("current-meta-group");

        $("#current-meta-actions").removeClass("disabled");

        // add the blank meta field
        var templateClone = $("#meta-template > div").clone();
        $(templateClone).removeAttr("id");
        $(".current-meta-item").removeClass("current-meta-item");
        $(templateClone).addClass("current-meta-item");
        // add correct names
        var metaKey = $(this).parents(".meta-group").find(".meta-list-item").length;
        var groupName = $(this).parents(".meta-group").find(".meta-group-name").val();
        $(templateClone).find("[data-name]").each(function() {
            var newName = $(this).attr("data-name").replace("metaGroup", groupName).replace("metaKey", metaKey);
            $(this).attr("name", newName);
        });

        //add to current group
        $(".current-meta-group .meta-list").append(templateClone);
    });

    $(document).on("click", ".meta-list-item", function() {
        $(".current-meta-item").removeClass("current-meta-item");
        $(this).addClass("current-meta-item");
    });

    // assign correct names if you change the meta group name
    $(document).on("input", ".meta-group-name", function() {
        reKey();
    });


    /**
     * Rekey all inputs within the meta
     * this isn't the most efficient way to do things
     * but is the easiest to keep track of and most centralized
     */
    reKey = function() {
        $(".meta-group").each(function() {
            var groupName = $(this).find(".meta-group-name").val();
            var metaItems = $(this).find(".meta-list-item");
            $(metaItems).each(function(metaKey) {
                $(this).find("[data-name]").each(function() {
                // base new name
                var newName = $(this).attr("data-name").replace("metaGroup", groupName).replace("metaKey", metaKey);

                //check for options as well
                if ($(this).hasClass("is-option-input")) {
                    var optionKey = $(this).parents(".options-list-item").index();
                    newName = newName.replace("optionKey", optionKey);
                }

                // check for extra fields
                if ($(this).hasClass("is-extra-field")) {
                    var extraFieldKey = $(this).parents(".extra-field").index() - 2;
                    newName = newName.replace("extraFieldKey", extraFieldKey);
                }

                // apply new name
                $(this).attr("name", newName);
            });
            });
        });
    }


    /****************************************************************
    * Editing meta
    * **************************************************************/

    /**
     * This is for editing meta once it's already created
     */
    // $(document).on("click", '[data-action="edit-meta"]', function() {
    openStills.editMeta = function(instance) {
        // grab all data to edit
        var parent = $(instance).parents(".meta-list-item");
        var metaKey = $(parent).attr("data-meta-key");
        var inputs = $(parent).find(".hidden-input-container input");

        // set the value of the meta we're editing
        $("#meta-template").attr("data-meta-key", metaKey);

        // loop through fill levels
        for (var i = 0; i < 3; i++) {
            $("#meta-template").find('[data-fill-order="' + i + '"]').each(function() {
                var type = $(this).attr("type");
                var name = $(this).attr("data-name");
                name = name.replace("meta[metaKey]", "meta[" + metaKey + "]");

                // check if it's in the input list
                if ($(parent).find('[name="' + name + '"]').length == 0) {
                    return true;
                }

                var value = $(parent).find('[name="' + name + '"]').val();
                // fill in values
                if (type == "radio") {
                    if ($(this).val() == value) {
                        $(this).attr("checked", true);
                    }
                } else {
                    $(this).val(value);
                }
                // for some reason trigger change didn't work
                $(this).trigger("input");
            });

            // adding options
            // we're going to take the easy way out
            // instead of rewriting all the html for the inputs
            // we're just going to put then in the input and then trigger the click
            // which already has all that jazz associated with it
            for (var i = 0; i < 5; i++) {
                var testKey = "meta[" + metaKey + "][options][" + i + "][label]";
                console.log($(parent).find('[name="' + testKey + '"]').length);
                if ($(parent).find('[name="' + testKey + '"]').length != 0) {
                    var labelVal = $(parent).find('[name="' + testKey + '"]').val();
                    var valueVal = $(parent).find('[name="meta[' + metaKey + '][options][' + i + '][value]"]').val();
                    $('#new-option [data-name="meta[metaKey][options][optionKey][label]"]').val(labelVal);
                    $('#new-option [data-name="meta[metaKey][options][optionKey][value]"]').val(valueVal);
                    $('[data-action="addSelectOption"]').click();
                } else {
                    i = 1000;
                }
            }

            $("#meta-template").removeClass("hidden");
        }
    };

    /****************************************************************
    * One off functions within meta
    * these are like adding options, object extra meta, etc
    * **************************************************************/

    /**
     * This is for adding select options
     */
    $("#options-list").sortable({
        stop: function(event, ui) {
            // rekey everything
            var optionsRegex = /\[options\]\[.\]/;
            // loop through all list items that we have
            $(".options-list-item").each(function(key) {
                // loop through all inputs within that list item
                var newOptionKey = "[options][" + key + "]";
                $(this).find("[data-name]").each(function() {
                    var name = $(this).attr("data-name");
                    var newName = name.replace(optionsRegex, newOptionKey);
                    $(this).attr("data-name", newName);
                });
            });
        }
    });

    //$(document).on("click", '[data-action="add-select-option"]', function() {
    openStills.addSelectOption = function(instance) {
        // get objects and variables
        var optionsList = $(instance).parents(".select-section").find(".options-list");
        var optionKey = $(optionsList).find(".options-list-item").length;
        var metaKey = $(instance).parents(".meta-list-item").index();
        var groupName = $(instance).parents(".meta-group").find(".meta-group-name").val();
        var elementNameStart = 'meta[' + groupName + '][' + metaKey + '][options][' + optionKey + ']';
        var dataNameStart = 'meta[metaGroup][metaKey][options][optionKey]';

        var optionLabel = $(instance).parents(".new-option").find(".new-option-label").val();
        $(instance).parents(".new-option").find(".new-option-label").val("");
        var optionValue = $(instance).parents(".new-option").find(".new-option-value").val();
        $(instance).parents(".new-option").find(".new-option-value").val("");

        // build html
        var html = '<div class="options-list-item columns is-grey-hover">';

        html += '<div class="option-label column is-5">';
        html += '<input type="text" class="input is-small is-option-input" name="' + elementNameStart + '[label]" data-name="' + dataNameStart + '[label]" value="' + optionLabel + '" />';
        html += '</div>';

        html += '<div class="option-label column is-5">';
        html += '<input type="text" class="input is-small is-option-input" name="' + elementNameStart + '[value]" data-name="' + dataNameStart + '[value]" value="' + optionValue + '" />';
        html += '</div>';

        html += '<div class="column is-2">';
        html += '<i class="fa fa-trash delete-option"></i>';
        html += '</div>';

        html += '</div>';

        // append the html
        $(optionsList).append(html);

    };

    /**
     * This is for deleting select options
     */
    // $(document).on("click", '[data-action="delete-option"]', function() {
    openStills.deleteOption = function(instance) {
        // remove html
        $(instance).parents(".options-list-item").remove();

        // rekey everything
        var optionsRegex = /\[options\]\[.\]/;
        // loop through all list items that we have
        $(".options-list-item").each(function(key) {
            // loop through all inputs within that list item
            var newOptionKey = "[options][" + key + "]";
            $(this).find("[data-name]").each(function() {
                var name = $(this).attr("data-name");
                var newName = name.replace(optionsRegex, newOptionKey);
                $(this).attr("data-name", newName);
            });
        });

    };

    /****************************************************************
    * Adding extra fields to object type
    * **************************************************************/

    // $(document).on("click", "[data-action='add-extra-field']", function() {
    openStills.addExtraField = function(instance) {
        var html = '<div class="extra-field column is-2">';
        html += '<input type="text" class="extra-field-name is-extra-field is-small is-size-7 has-text-weight-bold" value="Extra Field Label" data-name="meta[metaGroup][metaKey][extra-fields][extraFieldKey][label]" />';
        html += '<input type="text" class="extra-field-slug is-extra-field is-small is-size-7 has-text-grey" value="extra-field-slug" data-name="meta[metaGroup][metaKey][extra-fields][extraFieldKey][slug]" />';
        html += '</div>';

        $(instance).parents(".object-meta-section").find(".columns").append(html);

        // easiest to do this for now
        reKey();

    };

});


/********************************************************************
* Taxonomy functions
* ******************************************************************/

jQuery(document).ready(function($) {

    $("#taxonomy-list").sortable({
        stop: function( event, ui ) {
            var taxonomyRegex = /taxonomy\[.\]/;
            $(".taxonomy-list-item").each(function(key) {
                $(this).find("input").each(function() {
                    var name = $(this).attr("name");
                    var newName = name.replace(taxonomyRegex, "taxonomy[" + key + "]");
                    $(this).attr("name", newName);
                });
            });
        }
    });

    /**
     * New taxonomy
     */
    $("#new-taxonomy").click(function() {
        $("#taxonomy-template").removeClass("hidden");
    });

    // $('[data-action="add-taxonomy"]').click(function() {
    openStills.addTaxonomy = function(instance) {
        var taxonomyKey = $(".taxonomy-list-item").length;

        var html = '<div class="taxonomy-list-item columns">';

        html += '<div class="column is-5">';
        html += '<span class="title is-size-6">' + $('[data-name="taxonomy[taxonomyKey][label]"]').val() + '</span>';
        html += '</div>';

        html += '<div class="column is-5">';
        html += '<span class="title is-size-6">' + $('[data-name="taxonomy[taxonomyKey][slug]"]').val() + '</span>';
        html += '</div>';

        html += '<div class="column is-2 is-flex justify-end align-end">';
        html += '<a data-action="deleteTaxonomy" class="delete"></a>';
        html += '</div>';

        html += '<div class="hidden-input-container">';
        var labelName = "taxonomy[" + taxonomyKey + "][label]";
        html += '<input type="hidden" name="' + labelName + '" value="' + $('[data-name="taxonomy[taxonomyKey][label]"]').val() + '" />';
        var slugName = "taxonomy[" + taxonomyKey + "][slug]";
        html += '<input type="hidden" name="' + slugName + '" value="' + $('[data-name="taxonomy[taxonomyKey][slug]"]').val() + '" />';
        html += '</div>';

        html += '</div>';

        $("#taxonomy-list").append(html);

        // clear out
        $('[data-name="taxonomy[taxonomyKey][label]"]').val("");
        $('[data-name="taxonomy[taxonomyKey][slug]"]').val("");
    };


    /**
     * Delete taxonomy
     */
    // $(document).on("click", '[data-action="delete-taxonomy"]', function() {
    openStills.deleteTaxonomy = function(instance) {
        // remove it
        $(instance).parents(".taxonomy-list-item").remove();
        // rekey
        var taxonomyRegex = /taxonomy\[.\]/;
        $(".taxonomy-list-item").each(function(key) {
            // loop through inputs
            $(this).find("input").each(function() {
                var name = $(this).attr("name");
                var newName = name.replace(taxonomyRegex, "taxonomy[" + key + "]");
                $(this).attr("name", newName);
            });
        });
    };

});


/********************************************************************
* Namespace functions
* ******************************************************************/

jQuery(document).ready(function($) {

    // save a new namespace
    $("#save-namespace").click(function() {
        var namespace = $("#add-namespace-wrapper").find("input[type='text']").val();
        var regex = /^[a-zA-Zа-яА-Я0-9_!-]+$/;
        var instance = $(this);
        if (regex.test(namespace)) {
            $(this).addClass("is-loading");
            // create the namespace
            var apiUrl = "/wp-json/openstills/createnamespace";
            // send query
            $.post({
                url: apiUrl,
                data: {"namespace": namespace},
                success: (response) => {
                    console.log(response);
                    addSuccess("Your namespace has been created!");
                    var html = '<li>' + namespace + '</li>';
                    $("#namespaces-list ul").append(html);
                    $("#add-namespace-wrapper").find("input[type='text']").val("");
                },
                error: (response) => {
                    console.log(response);
                    // create messages html
                    addGenericError();
                }
            }).done(function() {
                $(instance).removeClass("is-loading");
            });
        } else {
            addGenericError("This is not an allowed namespace. Namespaces can only contain letters, numbers, dashes, and underscores.");
        }
    });

});



