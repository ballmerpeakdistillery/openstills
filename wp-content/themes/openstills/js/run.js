/********************************************************************
* Mash scripts
* ******************************************************************/

jQuery(document).ready(function($) {

    /**
     * change out labels, include timestamps
     */
    openStills.startRunCallback = function(instance) {

        $(instance).removeClass("is-primary");
        $(instance).addClass("is-danger");
        $(instance).text("End Run");

        // put in start time (might not 100% match server time)
        $('[data-stat="run-run-time"]').attr("data-start-time", Math.round(Date.now() / 1000));
        $('[data-stat="run-run-time"]').addClass("active").removeClass("new");

        // run timer if it's not already running somewhere
        if (!openStills.timerIsRunning) {
            openStills.runTimeUpdate();
        }

    }


    /**
     * End run actions
     */
    openStills.stopRunCallback = function(instance) {

        // styling
        $(instance).replaceWith('<span class="subtitle is-size-6">No available actions</span>');

        // add end time
        $('[data-stat="run-run-time"]').attr("data-stop-time", Math.round(Date.now() / 1000));

    }

});