/********************************************************************
* Subscription scripts
********************************************************************/

jQuery(document).ready(function($) {

    openStills.addedSubscriptionCallback = function(instance) {
        $(instance).toggleClass("added");
        $(instance).attr("data-action", "notification/removesubscription");
    }

    openStills.removeSubscriptionCallback = function(instance) {
        $(instance).toggleClass("added");
        $(instance).attr("data-action", "notification/addsubscription");
    }

});