<?php get_header(); ?>

	<main role="main">
	<!-- section -->
	<section>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="box">
				<h1 class="title"><?php the_title(); ?></h1>
				<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
						<?php the_post_thumbnail(); // Fullsize image for the single post ?>
				<?php endif; ?>

				<div class="notes">
					<h3 class="title is-size-6">Notes: </h3>
					<?php the_content(); ?>
				</div>

				<div id="bottling-report">
					<?php
					$trace = getTrace(get_the_ID());
					foreach ($trace as $traceId) {
						if ($traceId == 0) {
							continue;
						}

						// get base info
						$from = get_post_meta($traceId, "from", true);
						$to = get_post_meta($traceId, "to", true);

						// output by type
						$fromType = get_post_type($from);
						$toType = get_post_type($to);

						switch ($fromType) {
							case "mash":
								$vessel = getSingleMetaId($from, "vessel");
								?>
								<div class="report-item columns" data-type="mash">
									<div class="column is-3">
										<img src="<?php echo get_the_post_thumbnail_url($vessel); ?>" />
									</div>
									<div class="column is-9">
										<h3 class="title is-size-6 is-spaced">Mash: <?php echo get_the_title($from); ?></h3>
										<p class="subtitle is-size-6">
											Started: <?php echo date("M j, Y g:i A", get_post_meta($from, "start-time", true)); ?><br />
											Ended: <?php echo date("M j, Y g:i A", get_post_meta($from, "stop-time", true)); ?><br />
											<?php $recipe = getSingleMetaId($from, "recipe"); ?>
											Recipe: <?php echo get_the_title($recipe); ?>
										</p>
									</div>
								</div>
								<?php
								break;
							case "ferment":
								$vessel = getSingleMetaId($from, "vessel");
								?>
								<div class="report-item columns" data-type="ferment">
									<div class="column is-3">
										<img src="<?php echo get_the_post_thumbnail_url($vessel); ?>" />
									</div>
									<div class="column is-9">
										<h3 class="title is-size-6 is-spaced">Ferment: <?php echo get_the_title($from); ?></h3>
										<p class="subtitle is-size-6">
											Started: <?php echo date("M j, Y g:i A", get_post_meta($from, "start-time", true)); ?><br />
											Ended: <?php echo date("M j, Y g:i A", get_post_meta($from, "stop-time", true)); ?><br />
											Ending SG:
										</p>
									</div>
								</div>
								<?php
								break;
							case "run":
								$vessel = getSingleMetaId($from, "vessel");
								?>
								<div class="report-item columns" data-type="run">
									<div class="column is-3">
										<img src="<?php echo get_the_post_thumbnail_url($vessel); ?>" />
									</div>
									<div class="column is-9">
										<h3 class="title is-size-6 is-spaced">Run: <?php echo get_the_title($from); ?></h3>
										<p class="subtitle is-size-6">
											Type: <?php echo get_post_meta($from, "type", true); ?><br />
											Started: <?php echo date("M j, Y g:i A", get_post_meta($from, "start-time", true)); ?><br />
											Ended: <?php echo date("M j, Y g:i A", get_post_meta($from, "stop-time", true)); ?><br />
											Output Volume:
										</p>
									</div>
								</div>
								<?php
								break;
							default:

								break;
						}

						switch ($toType) {
							case "bottling":
								?>
								<div class="report-item columns is-multiline transfer" data-type="bottling">
									<div class="column is-12">
										<h3 class="title is-size-6 is-spaced">Recipe: Batch: </h3>
										<p class="subtitle is-size-6">
											Bottled On: <?php echo date("M j, Y g:i A", get_post_meta($traceId, "time", true)); ?><br />
											Proof: <br />
											Total Volume: <br />
											Bottles in Batch: <br />
										</p>
									</div>
									<div class="column is-12">
										<?php
										$bottleCount = get_post_meta($to, "bottle-count", true);
										for ($i = 0; $i < $bottleCount; $i++) {
											?><img src="<?php echo get_template_directory_uri(); ?>/img/bottle.png" /><?php
										}
										?>
									</div>
								</div>
								<?php
								break;
							case "vessel":
								$fromVessel = getSingleMetaId($from, "vessel");
								$time = get_post_meta($traceId, "time", true);
								?>
								<div class="report-item transfer">
									<div class="columns is-multiline">
										<div class="column is-12">
											<h3>
												<i class="fa fa-long-arrow-down"></i>
												Transferred from <strong><?php echo get_the_title($fromVessel); ?></strong> to <strong><?php echo get_the_title($to); ?></strong> on <strong><?php echo date("M j, Y", $time); ?></strong> at <strong><?php echo date("g:i A", $time); ?></strong>
											</h3>
										</div>
										<div class="column is-12">
											<img src="<?php echo get_the_post_thumbnail_url($to); ?>" />
										</div>
									</div>
								</div>
								<?php
								break;
							default:
								$fromData = json_decode(get_post_meta($from, "vessel", true));
								if ($fromData && !is_null($fromData)) {
									$fromVessel = getSingleMetaId($from, "vessel");
								} else {
									$fromVessel = $from;
								}
								$toVessel = getSingleMetaId($to, "vessel");
								$time = get_post_meta($traceId, "time", true);
								?>
								<div class="report-item transfer">
									<div class="columns is-multiline">
										<div class="column is-12">
											<h3 class="is-size-6">
												<i class="fa fa-long-arrow-down"></i>
												Transferred from <strong><?php echo get_the_title($fromVessel); ?></strong> to <strong><?php echo get_the_title($toVessel); ?></strong> on <strong><?php echo date("M j, Y", $time); ?></strong> at <strong><?php echo date("g:i A", $time); ?></strong>
											</h3>
										</div>
									</div>
								</div>
								<?php
								break;
						}
					}
					?>
				</div>

			</div>
			<div id="comments" class="box">
				<?php osComments(get_the_ID()); ?>
			</div>
		</article>
		<!-- /article -->
	<?php endwhile; ?>
	<?php else: ?>
		<!-- article -->
		<article>
			<h1><?php _e( 'Sorry, nothing to display.', 'openstills' ); ?></h1>
		</article>
		<!-- /article -->
	<?php endif; ?>
	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>
