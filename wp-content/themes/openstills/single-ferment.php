<?php get_header(); ?>

	<main role="main">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" class="columns is-multiline" data-mash-id="<?php the_ID(); ?>" data-post-id="<?php the_ID(); ?>">

			<div class="column is-12">
				<div class="box" id="mash-graph-wrapper">
					<h1 class="title is-size-4"><?php the_title(); ?></h1>
					<div class="columns" id="mash-actions">

						<?php $status = get_post_meta(get_the_ID(), "status", true); ?>
						<?php
						switch ($status) {
							case "active":
								?>
								<div class="column is-6">
									<a class="button is-danger is-fullwidth" data-action="ferment/stopferment" data-id="<?php the_ID(); ?>" data-callback="stopFermentCallback">Stop Ferment</a>
								</div>
								<?php
								break;
							case "ended":
								?>
								<div class="column is-6">
									<a class="button is-info is-fullwidth" href="/add/production/run/">New Stripping Run</a>
								</div>
								<?php
								break;
							default:
								break;
						}
						?>

						<!-- transfer stuff -->
						<?php
						$transferClass = "hidden";
						if ($status == "ended") {
							$transferClass = "";
						}
						?>
						<div class="transfer-wrapper column is-6 <?php echo $transferClass; ?>">
							<?php
							$args = array(
								'post_type'			=> 'vessel',
								'posts_per_page'	=> -1
							);
							$vessels = get_posts($args);
							?>
							<div class="select">
								<select id="transfer-options">
									<?php foreach ($vessels as $vessel): ?>
									<option value="<?php echo $post->ID; ?>"><?php echo $vessel->post_title; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<a class="button is-info" data-action="">
								<span>Transfer</span>
								<span class="icon"><i class="fa fa-exchange"></i></span>
							</a>
						</div>
						<!-- end transfer stuff -->

					</div>
				</div>
			</div>

			<?php
			// getting config values, moving that to JS
			$vessel = getSingleMetaId(get_the_ID(), "vessel");
			$sensors = getSensors($vessel);
			?>

			<!-- stats -->
			<div class="column is-12">
				<div class="stats-slider">
					<div class="stat time-stat <?php echo get_post_meta(get_the_id(), "status", true); ?>" data-stat="ferment-run-time" data-start-time="<?php echo get_post_meta(get_the_ID(), "start-time", true); ?>" data-stop-time="<?php echo get_post_meta(get_the_ID(), "stop-time", true); ?>" data-format="DD:HH:MM:SS">
						<div class="box">
							<label class="label has-text-weight-bold is-uppercase is-size-7">
								Run Time
								<span class="icon is-size-6">
									<i class="fa fa-clock-o"></i>
								</span>
							</label>
							<span class="value is-size-3 has-text-grey-light"></span>
						</div>
					</div>

					<?php foreach ($sensors as $sensorId => $sensorLabel): ?>
					<div class="stat" data-sensor-id="<?php echo $sensorId; ?>" data-post-id="<?php the_ID(); ?>">
						<div class="box">
							<i class="manual-input fa fa-hand-paper-o has-text-primary"></i>
							<label class="label has-text-weight-bold is-uppercase is-size-7">
								<?php echo $sensorLabel; ?>
							</label>
							<span class="value is-size-3 has-text-grey-light">--</span>
							<div class="manual-value-form">
								<form>
									<?php $sensorStringId = get_post_meta($sensorId, "sensor-id", true); ?>
									<input type="hidden" name="id" value="<?php echo $sensorStringId; ?>" />
									<div class="field has-addons">
										<div class="control">
											<input class="input" type="text" name="data">
										</div>
										<div class="control">
											<button type="submit" class="button is-primary" data-action="sensor/adddata" data-callback="manualSensorInputCallback">
												<i class="fa fa-check"></i>
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
					
				</div>
			</div>

			<!-- end stats -->

			<div class="column is-12">
				<div class="graph-wrapper">
					<canvas id="ferment-graph" class="graph" data-type="scatter" data-start-time="<?php echo get_post_meta(get_the_ID(), "start-time", true); ?>" data-stop-time="<?php echo get_post_meta(get_the_ID(), "stop-time", true); ?>" data-min="0" data-max="150" data-time-span="168"></canvas>
					<?php foreach ($sensors as $sensorId => $sensorLabel): ?>
					<div class="sensor-data" data-id="<?php echo $sensorId; ?>" data-label="<?php echo $sensorLabel; ?>"></div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="column is-12">
				<div class="box">
					<h3 class="subtitle has-text-weight-bold is-size-6">Notes:</h3>
					<?php the_content(); ?>
				</div>
			</div>

			<div class="column is-12" id="ferment-comments">
				<?php osComments(get_the_ID()); ?>
			</div>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'openstills' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>
