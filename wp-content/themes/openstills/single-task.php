<?php get_header(); ?>

    <main role="main">
        <!-- section -->
        <section>
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="box">
                    <div class="columns">
                        <div class="column is-6">
                            <h1 class="title is-size-5"><?php the_title(); ?></h1>
                        </div>
                        <div class="column is-6" id="task-actions">

                        </div>
                    </div>
                    <div class="columns is-multiline">
                        <div class="column is-4">
                            <span class="subtitle has-text-weight-bold is-size-6">Status:</span>
                            <?php
                            $statusClasses = array(
                                "new"           => "is-primary",
                                "in-progress"   => "is-warning",
                                "blocked"       => "is-danger",
                                "complete"      => "is-light"
                            );
                            $status = get_post_meta(get_the_ID(), "status", true);
                            ?>
                            <span class="button <?php echo $statusClasses[$status]; ?> is-small stat"><?php echo $status; ?></span>
                        </div>
                        <div class="column is-4">
                            <span class="subtitle has-text-weight-bold is-size-6">Priority:</span>
                            <?php
                            $priorityClasses = array(
                                "low"      => "is-primary",
                                "normal"   => "is-warning",
                                "high"     => "is-danger"
                            );
                            $priority = get_post_meta(get_the_ID(), "priority", true);
                            ?>
                            <span class="button <?php echo $priorityClasses[$priority]; ?> is-small stat"><?php echo $priority; ?></span>
                        </div>
                        <div class="column is-4">
                            <span class="subtitle has-text-weight-bold is-size-6">Category: </span>
                        </div>
                        <div class="column is-7">
                            <div id="task-content">
                                <span class="subtitle has-text-weight-bold is-size-6">Description:</span>
                                <div>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <?php osComments(get_the_ID()); ?>
                        </div>
                        <div class="column is-5">
                            <span class="subtitle has-text-weight-bold is-size-6">Due Date:</span>
                            <div class="calendar" data-style="compact" data-month="4" data-year="2019" data-selected-day="6-15-2019"></div>
                        </div>
                    </div>
                </div>
            </article>
            <!-- /article -->
        <?php endwhile; ?>
        <?php endif; ?>
        </section>
        <!-- /section -->
    </main>

<?php get_footer(); ?>
