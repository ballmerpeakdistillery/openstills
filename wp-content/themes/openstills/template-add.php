<?php
/**
 * Template Name: Add/Edit Items
 */
?>
<?php get_header(); ?>
<?php $postTypeObject = get_post_type_object($postType); ?>

<?php
// set up base information
// are we making a new post or editing
// what should the title be, etc

// default for new post
$isEdit = false;
$loadedPost = false;
$javascriptPostId = false;
$pageTitle = "Add New " . $postTypeObject->labels->singular_name;

// only set if we're editing a post and postId is set
if (isset($postId)):
	$isEdit = true;
	$loadedPost = get_post($postId);
	$javascriptPostId = $postId;
	$pageTitle = 'Edit <span class="has-text-info">' . $loadedPost->post_title . "</span>";
endif;
?>

<script>
	var namespace = "<?php echo $namespace; ?>";
	var postType = "<?php echo $postType; ?>";
	var postId = <?php echo $javascriptPostId ? $javascriptPostId : 'false'; ?>;
</script>

<!-- section -->
<section class="post-edit-wrapper">
	<h1 class="title"><?php echo $pageTitle; ?></h1>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<?php the_content(); ?>
		<form action="/wp-admin/post.php">
			<input type="hidden" id="user-id" name="user_ID" value="<?php echo get_current_user_id() ; ?>" />
			<input type="hidden" id="post_author" name="post_author" value="<?php echo get_current_user_id() ; ?>" />
			<input type="hidden" id="post_type" name="post_type" value="<?php echo esc_attr($postType); ?>" />
			<input type="hidden" id="original_post_status" name="original_post_status" value="publish" />
			<input type="hidden" id="status" name="status" value="publish" />

			<!-- keeping track of new vs updated status -->
			<input type="hidden" id="save-status" name="meta[save-status]" value="new" />

			<div id="htmldecoder"></div>

			<div class="columns is-2">
				<?php get_template_part('templates/add', $postType); ?>
			</div>
		</form>
		<br class="clear">

	<?php endwhile; ?>
	<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'openstills' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
</section>
<!-- /section -->

<?php get_footer(); ?>
