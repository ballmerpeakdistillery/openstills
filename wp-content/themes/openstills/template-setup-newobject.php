<?php
/**
 * Template Name: Setup - new object
 */
?>
<?php get_header(); ?>

<?php // setup any object data ?>
<?php if (isset($object)): ?>
<script>
    var namespace = '<?php echo $namespace; ?>';
    var object = '<?php echo $namespace . "/" . $object; ?>';
</script>
<?php endif; ?>
<!-- section -->
<section class="setup-wrapper">
    <div id="edit-object">
        <div class="is-loader is-size-1"></div>
        <form id="object-edit-form" action="" class="ajax-form">

            <h2 class="title is-spaced">
                <span id="page-title" class="title is-size-5">New Object</span>
                <div class="new-object-actions is-pulled-right">
                    <button class="button is-primary" type="submit">Save Object</button>
                </div>
            </h2>

            <h3 class="subtitle has-text-weight-bold is-uppercase has-text-info is-size-7">Basic Information</h3>
            <div class="new-object-section box">

                <div class="columns is-multiline">
                    <div class="column is-9">
                        <label class="label">Object Name</label>
                        <input type="text" class="input" name="config[labels][name]" />
                    </div>

                    <div class="column is-3">
                        <label class="label">Namespace</label>
                        <div class="select is-fullwidth">
                            <select name="namespace" class="is-fullwidth">
                                <?php $namespaces = getDefinedNamespaces(); ?>
                                <?php foreach ($namespaces as $namespace): ?>
                                <option value="<?php echo $namespace; ?>"><?php echo $namespace; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="column is-6">
                        <label class="label">Singular Name</label>
                        <input type="text" class="input" name="config[labels][singular_name]" />
                    </div>

                    <div class="column is-6">
                        <label class="label">Slug</label>
                        <input type="text" class="input" name="config[rewrite][slug]" />
                    </div>

                    <div class="column is-3">
                        <label class="label">Public</label>
                        <div class="control">
                            <label class="radio">
                                <input type="radio" name="config[public]:boolean" value="true" checked="checked" />
                                Yes
                            </label>
                            <label class="radio">
                                <input type="radio" name="config[public]:boolean" value="false" />
                                No
                            </label>
                        </div>
                    </div>

                    <div class="column is-3">
                        <label class="label">Has Archive</label>
                        <div class="control">
                            <label class="radio">
                                <input type="radio" name="config[has_archive]:boolean" value="true" checked="checked" />
                                Yes
                            </label>
                            <label class="radio">
                                <input type="radio" name="config[has_archive]:boolean" value="false" />
                                No
                            </label>
                        </div>
                    </div>

                    <div class="column is-4">
                        <label class="label">Show in REST (recommended)</label>
                        <div class="control">
                            <label class="radio">
                                <input type="radio" name="config[show_in_rest]:boolean" value="true" checked="checked" />
                                Yes
                            </label>
                            <label class="radio">
                                <input type="radio" name="config[show_in_rest]:boolean" value="false" />
                                No
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="is-flex">
                <h3 class="subtitle has-text-weight-bold is-uppercase has-text-info is-size-7">Meta Information</h3>
                <div id="meta-actions" class="is-flex">
                    <div id="current-meta-actions" class="not-selected disabled">
                            <i class="fa fa-caret-left decrement-size"></i>
                            <i class="fa fa-caret-right increment-size"></i>
                            <i class="fa fa-trash delete-meta-field"></i>
                    </div>
                    <div>
                        <span class="button is-primary is-small" id="new-meta-group">
                            <span class="icon">
                                <i class="fa fa-plus"></i>
                            </span>
                            <span>New Meta Group</span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="new-object-section box">

                <div id="meta-template" class="columns">
                    <div class="meta-list-item columns is-multiline column is-12 dependent-parent" data-span-size="12">
                        <div class="column is-6">
                            <input data-fill-order="0" data-name="meta[metaGroup][metaKey][label]" class="meta-name has-text-black is-fullwidth is-size-7" type="text" value="New Meta Field" />
                            <input data-fill-order="0" data-name="meta[metaGroup][metaKey][slug]"" class="meta-slug has-text-gray is-size-7 is-fullwidth" type="text" value="new-meta-field" />
                            <input data-name="meta[metaGroup][metaKey][meta-size]" type="hidden" class="meta-size" data-fill-order="0" value="is-12" />
                        </div>
                        <div class="column is-3">
                            <label class="label is-size-7">Type</label>
                            <div class="select is-small is-fullwidth">
                                <select id="new-meta-type" data-name="meta[metaGroup][metaKey][type]" class="has-dependents" data-dependency-key="meta-type" data-fill-order="0">
                                    <option>--None--</option>
                                    <option value="text">Text</option>
                                    <option value="textarea">Textarea</option>
                                    <option value="select">Select</option>
                                    <option value="object">Object</option>
                                    <option value="user">User</option>
                                </select>
                            </div>
                        </div>
                        <div class="column is-3">
                            <div class="is-pulled-right">
                                <label class="label is-size-7">Required</label>
                                <div class="toggle is-false">
                                    <input type="hidden" data-fill-order="0" data-name="meta[metaGroup][metaKey][required]" value="false" />
                                </div>
                            </div>
                        </div>

                        <!-- text dependent stuff -->
                        <div class="column object-meta-section is-12 is-dependent" data-dependent-on="meta-type" data-dependent-on-value="text">
                            <div>
                            <h3 class="subtitle has-text-weight-bold is-uppercase is-size-7">Text Field Properties</h3>
                                <div class="columns">
                                    <div class="column is-4">
                                        <label class="label is-size-7">Validation</label>
                                        <div class="select is-small">
                                            <select data-name="meta[metaGroup][metaKey][validation]" class="has-dependents" data-dependency-key="text-validation" data-fill-order="1">
                                                <option value="none">None</option>
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="email">Email</option>
                                                <option value="phone">Phone Number</option>
                                                <option value="custom">Custom</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="column is-4 is-dependent" data-dependent-on="text-validation" data-dependent-on-value="custom">
                                        <label class="label is-size-7">Custom Validation (regex)</label>
                                        <input class="input is-small" type="text" data-name="meta[metaGroup][metaKey][custom_validation]" data-fill-order="2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /text dependent stuff -->

                        <!-- select dependent stuff -->
                        <div class="column object-meta-section select-section is-12 is-dependent" data-dependent-on="meta-type" data-dependent-on-value="select">
                            <h3 class="subtitle has-text-weight-bold is-uppercase is-size-7">Select Options</h3>
                            <div class="options-list"></div>
                            <div class="columns new-option">
                                <div class="column is-5">
                                    <label class="label is-size-7">Label</label>
                                    <input type="text" class="input persistent-input is-small new-option-label" />
                                </div>
                                <div class="column is-5">
                                    <label class="label is-size-7">Value</label>
                                    <input type="text" class="input persistent-input is-small new-option-value" />
                                </div>
                                <div class="column is-2 is-flex justify-end align-end">
                                    <a data-action="addSelectOption" class="button is-primary is-small">
                                        <i class="fa fa-plus is-primary"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- /select dependent stuff -->

                        <!-- object dependent stuff -->
                        <div class="column object-meta-section is-12 is-dependent" data-dependent-on="meta-type" data-dependent-on-value="object">
                            <h3 class="subtitle has-text-weight-bold is-uppercase is-size-7">Object Properties</h3>
                            <div class="columns">
                                <div class="column is-3">
                                    <label class="label is-size-7">Object Type</label>
                                    <div class="select is-small">
                                        <select data-name="meta[metaGroup][metaKey][object_type]" data-fill-order="1">
                                            <option value="">-- None Selected --</option>
                                            <option value="any">Any (Not Recommended)</option>
                                            <?php
                                            $objects = getDefinedObjects();
                                            foreach ($objects as $object):
                                            ?>
                                            <option value="<?php echo $object; ?>"><?php echo $object; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="column is-2">
                                    <label class="label is-size-7">Extra Fields?</label>
                                    <span class="button is-small is-primary" data-action="addExtraField">
                                        <i class="fa fa-plus"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- list of meta groups -->
                <div id="meta-group-list"></div>

                <!-- list of already defined fields -->
                <div id="meta-field-list"></div>
            </div>

            <h3 class="subtitle has-text-weight-bold is-uppercase has-text-info is-size-7">
                Category Information
                <div class="category-actions is-pulled-right">
                    <span class="button is-primary is-small" id="new-taxonomy">
                        <span class="icon">
                            <i class="fa fa-plus"></i>
                        </span>
                        <span>New Category</span>
                    </span>
                </div>
            </h3>
            <div class="new-object-section box">
                <p class="note">NOTE: These are the list of top level categories. It's almost like another meta field. </p>
                <h3 class="subtitle has-text-weight-bold is-uppercase is-size-7">Category List</h3>
                <div id="taxonomy-list"></div>
                <div id="taxonomy-template" class="columns hidden">
                    <div class="column is-5">
                        <label class="label">Label</label>
                        <input type="text" class="input" data-name="taxonomy[taxonomyKey][label]" />
                    </div>
                    <div class="column is-5">
                        <label class="label">Slug</label>
                        <input type="text" class="input" data-name="taxonomy[taxonomyKey][slug]" />
                    </div>
                    <div class="column is-2 is-flex justify-end align-end">
                        <a class="button is-primary" data-action="addTaxonomy">
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>

        </form>
    </div>

    <?php
    /*
    <!-- New Meta Field Modal -->
    <div class="modal" id="meta-field-modal">
        <div class="modal-background"></div>
        <div class="modal-content">
            <div class="box">

                <div id="meta-template">
                    <h4 class="subtitle new-meta-title">
                        New Meta Field
                    </h4>
                    <div class="columns is-multiline">
                        <div class="column is-5">
                            <label class="label">Label</label>
                            <input id="new-meta-label" type="text" class="input" data-name="meta[metaKey][label]" data-fill-order="0" />
                        </div>
                        <div class="column is-3">
                            <label class="label">Key</label>
                            <input id="new-meta-key" type="text" class="input" data-name="meta[metaKey][key]" data-fill-order="0" />
                        </div>
                        <div class="column is-2">
                            <label class="label">Type</label>
                            <div class="select">
                                <select id="new-meta-type" data-name="meta[metaKey][type]" class="has-dependents" data-dependency-key="meta-type" data-fill-order="0">
                                    <option>--None--</option>
                                    <option value="text">Text</option>
                                    <option value="textarea">Textarea</option>
                                    <option value="select">Select</option>
                                    <option value="object">Object</option>
                                </select>
                            </div>
                        </div>
                        <div class="column is-2">
                            <label class="label">Required?</label>
                            <div class="control">
                                <label class="radio">
                                    <input type="radio" data-name="meta[metaKey][required]:boolean" value="true" data-fill-order="0" />
                                    Yes
                                </label>
                                <label class="radio">
                                    <input type="radio" data-name="meta[metaKey][required]:boolean" value="false" checked="checked" data-fill-order="0" />
                                    No
                                </label>
                            </div>
                        </div>

                        <!-- text dependent stuff -->
                        <div class="column object-meta-section is-12 is-dependent" data-dependent-on="meta-type" data-dependent-on-value="text">
                            <div>
                            <h3 class="subtitle has-text-weight-bold is-uppercase is-size-7">Text Field Properties</h3>
                                <div class="columns">
                                    <div class="column is-4">
                                        <label class="label">Validation</label>
                                        <div class="select">
                                            <select data-name="meta[metaKey][validation]" class="has-dependents" data-dependency-key="text-validation" data-fill-order="1">
                                                <option value="none">None</option>
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="email">Email</option>
                                                <option value="phone">Phone Number</option>
                                                <option value="custom">Custom</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="column is-4 is-dependent" data-dependent-on="text-validation" data-dependent-on-value="custom">
                                        <label class="label">Custom Validation (regex)</label>
                                        <input class="input" type="text" data-name="meta[metaKey][custom_validation]" data-fill-order="2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /text dependent stuff -->

                        <!-- select dependent stuff -->
                        <div class="column object-meta-section is-12 is-dependent" data-dependent-on="meta-type" data-dependent-on-value="select">
                            <div>
                                <h3 class="subtitle has-text-weight-bold is-uppercase is-size-7">Select Options</h3>
                                <div id="options-list"></div>
                                <div class="columns" id="new-option">
                                    <div class="column is-5">
                                        <label class="label">Label</label>
                                        <input type="text" class="input persistent-input" data-name="meta[metaKey][options][optionKey][label]" data-fill-order="1" />
                                    </div>
                                    <div class="column is-5">
                                        <label class="label">Value</label>
                                        <input type="text" class="input persistent-input" data-name="meta[metaKey][options][optionKey][value]" data-fill-order="1" />
                                    </div>
                                    <div class="column is-2 is-flex justify-end align-end">
                                        <a data-action="add-select-option" class="button is-primary">
                                            <i class="fa fa-plus is-primary"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /select dependent stuff -->

                        <!-- object dependent stuff -->
                        <div class="column object-meta-section is-12 is-dependent" data-dependent-on="meta-type" data-dependent-on-value="object">
                            <div>
                                <h3 class="subtitle has-text-weight-bold is-uppercase is-size-7">Object Properties</h3>
                                <div class="columns">
                                    <div class="column is-4">
                                        <label class="label">Object Type</label>
                                        <div class="select">
                                            <select data-name="meta[metaKey][object_type]" data-fill-order="1">
                                                <option value="">-- None Selected --</option>
                                                <option value="any">Any (Not Recommended)</option>
                                                <?php
                                                $objects = getDefinedObjects();
                                                foreach ($objects as $object):
                                                ?>
                                                <option value="<?php echo $object; ?>"><?php echo $object; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="column is-4">
                                        <label class="label">Extra Fields?</label>
                                        <div class="control">
                                            <label class="radio">
                                                <input type="radio" class="has-dependents" data-name="meta[metaKey][has_extra]" data-dependency-key="extra-fields" value="true" data-fill-order="1" />
                                                Yes
                                            </label>
                                            <label class="radio">
                                                <input type="radio" class="has-dependents" data-name="meta[metaKey][has_extra]" data-dependency-key="extra-fields" value="false" checked data-fill-order="1" />
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="is-dependent" data-dependent-on="extra-fields" data-dependent-on-value="true">
                                    <h3 class="subtitle is-size-7 is-uppercase">Extra Object Fields</h3>
                                    <div id="extra-field-list"></div>
                                    <div class="columns" id="new-extra-field">
                                        <div class="column is-4">
                                            <label class="label">Extra Field Label</label>
                                            <input type="text" class="input display-in-list" data-name="meta[metaKey][object_options][extraFieldKey][label]" data-fill-order="2">
                                        </div>
                                        <div class="column is-4">
                                            <label class="label">Extra Field Key</label>
                                            <input type="text" class="input display-in-list" data-name="meta[metaKey][object_options][extraFieldKey][key]" data-fill-order="2">
                                        </div>
                                        <div class="column is-2">
                                            <label class="label">Required?</label>
                                            <div class="control">
                                                <label class="radio">
                                                    <input type="radio" data-name="meta[metaKey][object_options][extraFieldKey][required]:boolean" value="true" data-fill-order="2" />
                                                    Yes
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" data-name="meta[metaKey][object_options][extraFieldKey][required]:boolean" value="false" data-fill-order="2" />
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                        <div class="column is-2 is-flex justify-end align-end">
                                            <a data-action="add-extra-field" class="button is-primary">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /object dependent stuff -->

                        <!-- Extra styling/attribute info -->
                        <div class="column is-12 object-meta-section">
                            <div>
                                <h3 class="subtitle has-text-weight-bold is-uppercase is-size-7">Extra Properties</h3>
                                <div class="columns">
                                    <div class="column is-4">
                                        <label class="label">Extra Classes</label>
                                        <input type="text" class="input" data-name="meta[metaKey][classes]" data-fill-order="0" />
                                    </div>
                                    <div class="column is-4">
                                        <label class="label">Extra Attributes</label>
                                        <input type="text" class="input" data-name="meta[metaKey][attributes]" data-fill-order="0" />
                                    </div>
                                    <div class="column is-4">
                                        <label class="label">Wrapper Classes</label>
                                        <input type="text" class="input" data-name="meta[metaKey][wrapper_classes]" data-fill-order="0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Extra styling/attribute info -->

                    </div>
                    <a class="button is-primary is-pulled-right" id="save-meta">
                        <span class="icon">
                            <i class="fa fa-plus"></i>
                        </span>
                        <span>Save Meta Field</span>
                    </a>
                </div>
                <!-- / meta template -->
            </div>
        </div>
        <button class="modal-close is-large" aria-label="close"></button>
    </div>
    */
   ?>

</section>
<!-- /section -->

<?php get_footer(); ?>
