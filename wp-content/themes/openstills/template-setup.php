<?php
/**
 * Template Name: Setup
 */
?>
<?php get_header(); ?>


<!-- section -->
<section class="setup-wrapper">

    <div class="box">
        <h3 class="title is-size-5">General Settings</h3>
        <p class="subtitle is-size-7 has-text-grey">Manage your experience within OpenStills from this section. For more detailed settings and OpenStills setup, dig a little deeper in the secitons below</p>
    </div>

    <div class="box">
        <div class="columns">
            <div class="column is-9">
                <h3 class="title is-size-5">Objects</h3>
                <p class="subtitle is-size-7 has-text-grey">Create, edit, and manage all defined objects from this area.</p>
            </div>
            <div class="column is-3">
                <a class="button is-primary is-pulled-right" href="/settings/objects/">Manage</a>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="columns">
            <div class="column is-9">
                <h3 class="title is-size-5">Menus</h3>
                <p class="subtitle is-size-7 has-text-grey">Manage your application menu</p>
            </div>
            <div class="column is-3">
                <a class="button is-primary is-pulled-right" href="/settings/menu/">Manage</a>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="columns">
            <div class="column is-9">
                <h3 class="title is-size-5">Subscriptions</h3>
                <p class="subtitle is-size-7 has-text-grey">Manage what you get notifications for</p>
            </div>
            <div class="column is-3">
                <a class="button is-primary is-pulled-right" href="/settings/subscriptions/">Manage</a>
            </div>
        </div>
    </div>
    <?php
    /*
	<div class="tabs-wrapper">
        <!-- setup navigation -->
        <div class="tabs">
            <ul>
                <li class="is-active tab" data-target="settings"><a>Settings</a></li>
                <li class="tab" data-target="objects"><a>Objects</a></li>
            </ul>
        </div>

        <!-- settings -->
        <div class="tab-target is-active" data-target="settings">
            <h2 class="title is-size-4">Settings</h2>
        </div>

        <!-- objects -->
        <div class="tab-target" data-target="objects">
            <h2 class="title is-size-4">Objects</h2>
            <div class="object-list columns is-multiline">
                <div class="is-3 column">
                    <a href="/setup/new-object" class="button is-medium is-fullwidth is-primary" id="new-object-action">
                        <span class="icon">
                            <i class="fa fa-plus"></i>
                        </span>
                        <span>New Object</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    */
   ?>

</section>
<!-- /section -->

<?php get_footer(); ?>
