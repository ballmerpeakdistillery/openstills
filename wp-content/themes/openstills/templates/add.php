
<div class="column is-8">
    <section class="box">
        <div class="field">
            <div class="control">
                <input class="input" type="text" name="title" placeholder="Title" />
            </div>
        </div>
        <div class="field">
            <div class="control">
                <textarea class="textarea is-fullwidth" name="content" placeholder="Notes (optional)"></textarea>
            </div>
        </div>
    </section>
    <?php $postTypeFile = file_get_contents(get_template_directory() . "/objects/" . $namespace . "/" . $postType . "-config.json"); ?>
    <?php $postTypeData = json_decode($postTypeFile, true); ?>
    <!-- meta -->
    <div id="post-meta-container">

        <?php foreach ($postTypeData["meta"] as $metaGroupTitle => $metaGroupFields): ?>
        <div class="meta-group box">
            <h3 class="title is-size-6"><?php echo $metaGroupTitle; ?></h3>
            <div class="columns is-multiline">
                <?php foreach ($metaGroupFields as $metaField): ?>
                <div class="column <?php echo $metaField["meta-size"]; ?>">
                    <label class="label"><?php echo $metaField["label"]; ?></label>
                    <?php
                    switch ($metaField["type"]) {
                        case "object":
                            ?>
                            <div class="field">
                                <ul class="selected-post-list">

                                </ul>
                                <div class="input-wrapper post-input-wrapper" data-post-type="<?php echo $metaField["object_type"]; ?>">
                                    <div class="extra-fields-list">
                                        <?php if (array_key_exists("extra-fields", $metaField)): ?>
                                        <?php foreach ($metaField["extra-fields"] as $field): ?>
                                        <div class="extra-field" data-label="<?php echo $field["label"]; ?>" data-slug="<?php echo $field["slug"]; ?>"></div>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <p class="control has-icons-right">
                                        <input class="post-input-hidden-input" type="hidden" name="meta[<?php echo $metaField["slug"]; ?>]" data-meta-type="post" />
                                        <input placeholder="Type To Search" class="post-search-input" />
                                        <span class="icon is-right action post-input-list-action">
                                            <i class="fa fa-list-ul has-text-primary"></i>
                                        </span>
                                    </p>
                                    <div class="post-list panel">

                                    </div>
                                </div>
                            </div>
                            <?php
                            break;
                        case "textarea":
                            echo '<textarea class="textarea is-fullwidth" name="meta[' . $metaField["slug"] . ']"></textarea>';
                            break;
                        case "select":
                            echo '<div class="select">';
                            echo '<select name="meta[' . $metaField["slug"] . ']">';
                            echo '<option>-- none selected --</option>';
                            foreach ($metaField["options"] as $option):
                                echo '<option value="' . $option["value"] . '">' . $option["label"] . '</option>';
                            endforeach;
                            echo '</select>';
                            echo '</div>';
                            break;
                        case "user":
                            $users = get_users();
                            echo '<div class="select"><select name="meta[' . $metaField["slug"] . ']">';
                            foreach ($users as $user) {
                                echo '<option value="' . $user->ID . '">' . $user->user_login . '</option>';
                            }
                            echo '</select></div>';
                            break;
                        default:
                            echo '<input type="text" class="input" name="meta[' . $metaField["slug"] . ']" />';
                            break;
                    }
                    ?>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>

<div class="column is-4">
    <section class="box">
        <div class="post-actions">
            <a class="button is-fullwidth is-medium is-primary" data-action="saveObject">
                Save
            </a>
        </div>
        <div id="post-taxonomies">
            <?php $taxonomies = get_object_taxonomies($postType, "object"); ?>
            <?php foreach ($taxonomies as $taxonomy): ?>
                <div class="taxonomy-wrapper" data-taxonomy-name="<?php echo $taxonomy->name; ?>" data-taxonomy-id="<?php echo $taxonomy->id; ?>">
                    <span class="subtitle is-size-7 is-uppercase has-text-weight-bold has-text-info">
                        <?php echo $taxonomy->label; ?>
                    </span>
                    <ul class="term-list">
                        <?php
                        $terms = get_terms( array(
                            'taxonomy' => $taxonomy->name,
                            'hide_empty' => false,
                        ));
                        ?>
                        <?php foreach ($terms as $term): ?>
                        <li>
                            <label class="checkbox">
                                <input type="checkbox" class="taxonomy-checkbox" value="<?php echo $term->term_id; ?>">
                                <?php echo $term->name; ?>
                            </label>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    <div class="field has-addons">
                        <div class="control is-fullwidth">
                            <input class="input is-small term-name-input" type="text" placeholder="New <?php echo $taxonomy->label; ?> Item">
                        </div>
                        <div class="control">
                            <a class="button is-primary is-small" data-action="addTerm"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </section>
</div>


<?php
// get post type
/*
$postObj = new $postType;
$postTypeObject = get_post_type_object($postType);

// get fields
$postConfig = $postObj->getConfig();
?>

<div class="column is-8">
    <section class="box">
    <?php
    // loop through
    foreach ($postConfig as $section => $fields):
        ?>
        <div class="edit-section">
            <span class="subtitle is-size-7 is-uppercase has-text-weight-bold has-text-info"><?php echo $section; ?></span>
            <?php foreach ($fields as $field): ?>
                <?php
                // we're using this instead of get_template_part so we can pass data to it
                // not sure how hacky it is, but it works
                $path = getTemplatePath("templates/inputs/input", $field["type"]);
                include($path);
                ?>
            <?php endforeach; ?>
        </div>
        <?php
    endforeach; ?>
    </section>
</div>

<div class="column is-4">
    <section class="box">
        <div class="post-actions">
            <a class="button is-fullwidth is-medium is-primary" data-action="submit" disabled>
                Save <?php echo $postTypeObject->labels->singular_name; ?>
            </a>
        </div>
        <div id="post-taxonomies">
            <?php $taxonomies = get_object_taxonomies($postType, "object"); ?>
            <?php foreach ($taxonomies as $taxonomy): ?>
                <div class="taxonomy-wrapper" data-taxonomy-name="<?php echo $taxonomy->name; ?>" data-taxonomy-id="<?php echo $taxonomy->id; ?>">
                    <span class="subtitle is-size-7 is-uppercase has-text-weight-bold has-text-info">
                        <?php echo $taxonomy->label; ?>
                    </span>
                    <ul class="term-list">
                        <?php
                        $terms = get_terms( array(
                            'taxonomy' => $taxonomy->name,
                            'hide_empty' => false,
                        ));
                        ?>
                        <?php foreach ($terms as $term): ?>
                        <li>
                            <label class="checkbox">
                                <input type="checkbox" name="category" value="<?php echo $term->term_id; ?>">
                                <?php echo $term->name; ?>
                            </label>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endforeach ?>
        </div>
    </section>
</div>
*/
