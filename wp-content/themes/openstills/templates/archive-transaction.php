<div id="transaction-stats">
    <div class="box">
        <span id="account-ballance">
            <?php
            $accountBalance = get_option("osAccount_balance");
            $totalClass = "success";
            if ($accountBalance < 0) {
                $totalClass = "danger";
            }
            ?>
            <span>Account Balance:<span>
            <span class="has-text-<?php echo $totalClass; ?>">
                $ <?php echo $accountBalance; ?>
            </span>
        </span>
        <span id="month-transactoins">
            <span>Transactions in <?php echo date("F"); ?>:</span>
            <span></span>
        </span>
        <span id="ytd-transactions">
            <span>YTD Transactions:</span>
            <span></span>
        </span>
    </div>
</div>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <div class="box">
        <article id="post-<?php the_ID(); ?>" class="columns">

            <div class="column is-4 post-title">
                <h2 class="is-size-5">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                </h2>
            </div>
            <div class="column is-3 transaction-date">
                <?php echo get_the_date("m/d/Y", get_the_ID()); ?>
            </div>
            <div class="column is-3 transaction-amount">
                <?php
                $amountClass = "success";
                $transactionType = get_post_meta(get_the_ID(), "type", true);
                if ($transactionType == "debit") {
                    $amountClass = "danger";
                }
                ?>
                <span class="has-text-weight-bold has-text-<?php echo $amountClass; ?>"><?php echo get_post_meta(get_the_ID(), "amount", true); ?></span><br />
                <?php
                $merchantId = getSingleMetaId(get_the_ID(), "merchant");
                $merchantObject = get_post($merchantId);
                ?>
                <span>
                    <a href="<?php echo get_the_permalink($merchantObject->ID); ?>"><?php echo $merchantObject->post_title; ?></a>
                </span>
            </div>

            <div class="column is-2 has-text-right is-size-5 post-actions">
                <a class="icon" href="/edit/<?php echo get_queried_object()->rewrite["slug"]?>/<?php echo get_the_ID(); ?>">
                    <i class="fa fa-pencil"></i>
                </a>
                <a data-action="delete" class="icon" href=""><i class="fa fa-trash"></i></a>
            </div>

        </article>
    </div>

<?php endwhile; ?>
<?php else: ?>

    <!-- article -->
    <article>
        <h2><?php _e( "No objects have been created yet.", 'openstills' ); ?></h2>
    </article>
    <!-- /article -->

<?php endif; ?>
