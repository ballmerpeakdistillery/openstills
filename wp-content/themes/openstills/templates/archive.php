<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<div class="box">
		<article id="post-<?php the_ID(); ?>" class="columns">

			<div class="column is-10 post-title">
				<h2 class="is-size-5">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h2>
			</div>

			<div class="column is-2 has-text-right is-size-5 post-actions">
				<a class="icon" href="/edit/<?php echo get_queried_object()->rewrite["slug"]?>/<?php echo get_the_ID(); ?>">
					<i class="fa fa-pencil"></i>
				</a>
				<a data-action="delete" class="icon" href=""><i class="fa fa-trash"></i></a>
			</div>

		</article>
	</div>

<?php endwhile; ?>
<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( "No objects have been created yet.", 'openstills' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
