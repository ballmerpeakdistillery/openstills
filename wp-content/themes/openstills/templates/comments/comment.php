<li class="comment-list-item">
    <div class="comment-meta">
        <span class="user has-text-weight-bold has-text-info">
            <?php
            $userData = get_userdata($comment->post_author);
            echo $userData->user_login;
            ?>
        </span>
        <span class="time has-text-grey-light">
            <?php
            $postTime = new DateTime($comment->post_date);
            $diffTime = $postTime->diff(new DateTime());

            if ($diffTime->days > 0) {
                if ($diffTime->y > 0) {
                    // include year
                    echo $postTime->format('M dd YY');
                } else {
                    // don't include year
                    echo $postTime->format('M dd');
                }
            } else if ($diffTime->h > 0) {
                echo $diffTime->h . "h";
            } else {
                echo $diffTime->i . "m";
            }
            ?>
        </span>
    </div>
    <div class="comment-content has-text-grey-dark">
        <?php echo $comment->post_content; ?>
    </div>
</li>