<div id="comment-form-wrapper" class="hidden">
    <form action="/wp-json/wp/v2/comment" id="comment-form">

        <input type="hidden" id="user-id" name="user_ID" value="<?php echo get_current_user_id() ; ?>" />
        <input type="hidden" id="post_author" name="post_author" value="<?php echo get_current_user_id() ; ?>" />
        <input type="hidden" id="post_type" name="post_type" value="comment" />
        <input type="hidden" id="original_post_status" name="original_post_status" value="publish" />
        <input type="hidden" id="status" name="status" value="publish" />
        <input type="hidden" id="parent" name="parent" value="<?php echo $parentId; ?>" />

        <input class="input" type="hidden" name="title" value="<?php echo createId(20); ?>" />

        <div class="field">
            <div class="control">
                <textarea rows="3" class="textarea" name="content"></textarea>
            </div>
        </div>
        <div class="field">
            <div class="control">
                <button class="button is-primary" type="submit" data-action="saveComment">
                    <span>Submit</span>
                    <span class="icon">
                        <i class="fa fa-comment"></i>
                    </span>
                </button>
            </div>
        </div>
    </form>
</div>