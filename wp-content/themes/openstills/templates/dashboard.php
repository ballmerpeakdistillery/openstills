<div id="dashboard">
    <div class="columns is-multiline" id="dashboard-items"><?php getDashboardItems(); ?></div>
    <div id="add-dashboard-item">
        <select id="dashboard-select">
            <option>-- None Selected --></option>
            <?php
            $args = array(
                "post_type"         => "dashboard-item",
                "posts_per_page"    => -1
            );
            $availableItems = get_posts($args);
            foreach ($availableItems as $dashboardItem):
                echo '<option value="' . $dashboardItem->ID . '">' . $dashboardItem->post_title . '</option>';
            endforeach;
            ?>
        </select>
        <a class="button is-primary is-small" data-action="addDashboardItem">
            <span>Add Item</span>
            <span class="icon"><i class="fa fa-plus"></i>
        </a>
        <a class="button is-primary is-small is-pulled-right" data-action="saveDashboard">Save Dashboard</a>
    </div>
</div>