<div id="dashboard-active-processes" class="dashboard-item column is-12" data-id="<?php echo $itemId; ?>">

    <h2 class="title is-size-6">Active Processes:</h2>

    <?php
    $args = array(
        "post_type"     => array(
            "mash",
            "ferment",
            "run"
        ),
        "meta_query"    => array(
            array(
                "key"       => "status",
                "value"     => "active"
            )
        )
    );
    $posts = get_posts($args);
    ?>

    <?php if (count($posts) == 0): ?>
    <span class="title is-size-6">There are no active processes right now</span>
    <?php else: ?>

    <div class="tabs-wrapper">
        <!-- tabs -->
        <div class="tabs">
            <ul>
                <?php foreach ($posts as $key => $post): ?>
                <?php
                $class = "";
                if ($key == 0) {
                    $class = "is-active";
                }
                ?>
                <li class="tab <?php echo $class; ?>" data-target="<?php echo $post->ID; ?>"><a><?php echo $post->post_title; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>

        <!-- tab items -->
        <div class="tab-items">
            <?php foreach ($posts as $key => $post): ?>
            <?php
            // getting config values, moving that to JS
            $vessel = getSingleMetaId($post->ID, "vessel");
            $sensors = getSensors($vessel);
            ?>
            <div class="tab-item tab-target" data-target="<?php echo $post->ID; ?>">
                <!-- stats -->
                <div class="column is-12">
                    <div class="stats-slider">
                        <div class="stat time-stat <?php echo get_post_meta($post->ID, "status", true); ?>" data-stat="ferment-run-time" data-start-time="<?php echo get_post_meta($post->ID, "start-time", true); ?>" data-stop-time="<?php echo get_post_meta($post->ID, "stop-time", true); ?>" data-format="DD:HH:MM:SS">
                            <div class="box">
                                <label class="label has-text-weight-bold is-uppercase is-size-7">
                                    Run Time
                                    <span class="icon is-size-6">
                                        <i class="fa fa-clock-o"></i>
                                    </span>
                                </label>
                                <span class="value is-size-3 has-text-grey-light"></span>
                            </div>
                        </div>

                        <?php foreach ($sensors as $sensorId => $sensorLabel): ?>
                        <div class="stat" data-sensor-id="<?php echo $sensorId; ?>" data-post-id="<?php echo $post->ID; ?>">
                            <div class="box">
                                <label class="label has-text-weight-bold is-uppercase is-size-7">
                                    <?php echo $sensorLabel; ?>
                                </label>
                                <span class="value is-size-3 has-text-grey-light">--</span>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="column is-12">
                    <div class="graph-wrapper">
                        <canvas id="ferment-graph" class="graph" data-type="scatter" data-start-time="<?php echo get_post_meta($post->ID, "start-time", true); ?>" data-stop-time="<?php echo get_post_meta($post->ID, "stop-time", true); ?>" data-min="0" data-max="150" data-time-span="168"></canvas>
                        <?php foreach ($sensors as $sensorId => $sensorLabel): ?>
                        <div class="sensor-data" data-id="<?php echo $sensorId; ?>" data-label="<?php echo $sensorLabel; ?>"></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <!-- / tab items -->
        </div>
    </div>
    <?php endif; ?>

</div>