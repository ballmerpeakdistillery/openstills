<div id="dashboard-latest-tasks" class="dashboard-item column is-4" data-id="<?php echo $itemId; ?>">
    <div class="box">
        <h2 class="title is-size-6">Latest Tasks:</h2>
        <div id="dashboard-latest-tasks">
            <?php
            $args = array(
                "post_type"         => "task",
                "posts_per_page"    => 3,
                "post_status"       => "new"
            );
            $tasks = get_posts($args);
            foreach ($tasks as $task):
                ?>
                <div class="dashboard-task-item">
                    <div class="is-pulled-left">
                        <span class="status button is-small is-primary">New</span>
                    </div>
                    <div class="is-pulled-left">
                        <span class="task-title is-size-7 has-text-weight-bold">
                            <a href="<?php echo get_the_permalink($task->ID); ?>"><?php echo $task->post_title; ?></a>
                        </span>
                    </div>
                </div>
                <?php
            endforeach;
            ?>
        </div>
        <h2 class="title is-size-6">Due Today:</h2>
        <div id="dashboard-tasks-due-today">

        </div>
        <?php $currentUser = wp_get_current_user(); ?>
        <a href="/tasks/<?php echo $currentUser->user_login; ?>/">See All Tasks</a>
    </div>
</div>
<style>
    #dashboard-latest-tasks .title {
        margin-bottom: 7px;
    }
    .dashboard-task-item {
        margin-bottom: 4px;
        overflow: hidden;
        width: 100%;
    }
    .dashboard-task-item .status {
        margin-right: 7px;
    }

    #dashboard-latest-tasks {
        margin-bottom: 15px;
    }
</style>