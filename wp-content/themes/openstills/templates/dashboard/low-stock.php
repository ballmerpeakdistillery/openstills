<div id="dashboard-low-stock-items" class="dashboard-item column is-4" data-id="<?php echo $itemId; ?>">
    <div class="box">
        <h2 class="title is-size-6">Low Stock Items:</h2>
        <div id="lowstock-header" class="columns">
            <div class="column is-8 has-text-weight-bold">
                Item:
            </div>
            <div class="column is-4 has-text-weigh-bold">
                Qty:
            </div>
        </div>
        <?php
        $args = array(
            "post_type"     => "supply",
            "meta_query"    => array(
                array(
                    "key"       => "status",
                    "value"     => "low-stock"
                )
            )
        );
        $supplies = get_posts($args);
        foreach ($supplies as $supply):
        ?>
        <div class="low-stock-item columns">
            <span class="supply-title column is-8"><?php echo $supply->post_title; ?></span>
            <span class="supply-inventory column is-4 has-text-danger">
                <?php echo get_post_meta($supply->ID, "current-inventory", true); ?>
                <?php echo get_post_meta($supply->ID, "supply-unit", true); ?>
            </span>
        </div>
        <?php endforeach; ?>
    </div>
</div>

<style>
#dashboard-low-stock-items .column {
    padding: 0;
}
#dashboard-low-stock-items .low-stock-item {
    margin-bottom: 0;
}
</style>