<?php
/**
 * Default input type renders as text input
 */
?>

<?php
$classes = getFieldClasses($field, "post-input input");
$attributes = getFieldAtts($field);
$wrapperClasses = getWrapperClasses($field, "field");
?>

    <label class="label"><?php echo $field["label"]; ?></label>
    <ul class="selected-post-list">

    </ul>
    <div class="input-wrapper post-input-wrapper" data-post-type="<?php echo $field["args"]["post_type"]; ?>">
        <p class="control has-icons-right">
            <input class="post-input-hidden-input" type="hidden" name="<?php echo $field["args"]["wp_field"]; ?>" data-meta-type="post" />
            <input 
            class="<?php echo $classes; ?>" 
            type="text" 
            placeholder="Type To Search" 
            <?php echo $attributes; ?>
            />
            <span class="icon is-right action post-input-list-action">
                <i class="fa fa-list-ul has-text-primary"></i>
            </span>
        </p>
        <div class="post-list panel">

        </div>
    </div>