<?php
/**
 * Textarea input type
 */
?>

<?php
$classes = getFieldClasses($field);
$attributes = getFieldAtts($field);
$wrapperClasses = getWrapperClasses($field, "field");
?>

<div class="<?php echo $wrapperClasses; ?>">
    <label class="label"><?php echo $field["label"]; ?></label>
    <div class="input-wrapper">
        <div class="select">
            <select 
                name="<?php echo $field["args"]["wp_field"]; ?>" 
                class="<?php echo $classes; ?>" 
                <?php echo $attributes; ?>
                >
                <?php foreach ($field["args"]["options"] as $optionValue => $optionLabel): ?>
                    <option value="<?php echo $optionValue; ?>"><?php echo $optionLabel; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</div>