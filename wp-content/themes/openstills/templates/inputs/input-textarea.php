<?php
/**
 * Textarea input type
 */
?>

<?php
$classes = getFieldClasses($field, "textarea");
$attributes = getFieldAtts($field);
$wrapperClasses = getWrapperClasses($field, "field");
?>

<div class="<?php echo $wrapperClasses; ?>">
    <label class="label"><?php echo $field["label"]; ?></label>
    <div class="input-wrapper">
        <textarea 
        class="<?php echo $classes; ?>" 
        name="<?php echo $field["args"]["wp_field"]; ?>" 
        <?php echo $attributes; ?>
        ></textarea>
    </div>
</div>