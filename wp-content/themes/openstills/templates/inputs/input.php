<?php
/**
 * Default input type renders as text input
 */
?>

<?php
$classes = getFieldClasses($field, "input");
$attributes = getFieldAtts($field);
$wrapperClasses = getWrapperClasses($field, "field");
?>

<div class="<?php echo $wrapperClasses; ?>">
    <label class="label"><?php echo $field["label"]; ?></label>
    <div class="input-wrapper">
        <input 
        class="<?php echo $classes; ?>" 
        type="text" 
        name="<?php echo $field["args"]["wp_field"]; ?>" 
        <?php echo $attributes; ?>
        />
    </div>
</div>