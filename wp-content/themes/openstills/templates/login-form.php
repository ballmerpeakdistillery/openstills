<div class="columns">

    <div class="column is-4">

    </div>
    <div class="column is-4">
        <div class="box" id="login-form-wrapper">
            <h2 class="title is-size-5 is-uppercase has-text-centered">User Login</h2>
            <p class="is-size-7">If you do not have an account, please contact your manager to get an account created for you</p>
            <form name="loginform" id="loginform" action="/wp-login.php" method="post">
                <div class="control has-icons-left">
                    <input type="text" name="log" id="user_login" class="input" placeholder="Username" value="" size="20">
                    <span class="icon is-small is-left">
                        <i class="fa fa-user"></i>
                    </span>
                </div>
                <div class="control has-icons-left">
                    <input type="password" name="pwd" id="user_pass" class="input" placeholder="Password" value="" size="20">
                    <span class="icon is-small is-left">
                        <i class="fa fa-lock"></i>
                    </span>
                    <label class="checkbox">
                        <input name="rememberme" type="checkbox" id="rememberme" value="forever">
                        Remember Me
                    </label>
                </div>
                <input type="submit" name="wp-submit" class="button is-primary is-fullwidth" value="Log In">
            </form>
        </div>
    </div>
</div>