<ul class="menu-list">
    <li class="menu-item">
        <a href="/"><i class="fa fa-dashboard"></i> Dasboard</a>
    </li>
    <?php $currentUser = wp_get_current_user(); ?>
    <li class="menu-item">
        <a href="/tasks/<?php echo $currentUser->user_login; ?>/">
            <i class="fa fa-list-alt"></i> 
            Tasks
            <span id="task-count" class="is-pulled-right is-size-7"><?php echo getTaskCount(); ?></span>
        </a>
    </li>
    <li class="menu-item">
        <a href="/reports/">
            <i class="fa fa-file-text-o"></i> Reports
        </a>
    </li>
</ul>