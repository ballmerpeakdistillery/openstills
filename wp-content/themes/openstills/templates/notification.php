<li class="notification-list-item">
    <label class="label has-text-grey-darker is-size-6"><?php echo $notification->post_title; ?></label>
    <p class="has-text-grey is-size-7"><?php echo $notification->post_content; ?></p>
    <div class="actions">
        <span class="has-text-link is-size-7" data-id="<?php echo $notification->ID; ?>" data-action="notification/markasread" data-callback="removeNotification">Mark as Read</span>
    </div>
</li>