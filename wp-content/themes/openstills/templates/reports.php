<div id="reports-wrapper">



    <div id="my-test">
        <script>

        </script>
    </div>




    <div class="columns">
        
        <div class="column is-7">
            <div class="box">
                <h2 class="title is-size-6 has-text-weight-bold">Recent Reports</h2>

                <div class="report-item-wrapper columns is-multiline">
                    <div class="column is-9">
                        <span class="subtitle is-size-7 has-text-weight-bold">Title</span>
                    </div>
                    <div class="column is-3">
                        <span class="subtitle is-size-7 has-text-weight-bold">Type</span>
                    </div>
                    <?php
                    $args = array(
                        "post_type"     => "report",
                    );
                    $reports = get_posts($args);
                    foreach ($reports as $report):
                        ?>
                            <div class="column is-9">
                                <a href="<?php echo get_the_permalink($report->ID); ?>"><?php echo $report->post_title; ?></a>
                            </div>
                            <div class="column is-3">
                                <?php echo get_post_meta($report->ID, "type", true); ?>
                            </div>
                        <?php
                    endforeach;
                    ?>
                </div>

            </div>
        </div>

        <div class="column is-5">
            <div class="box">
                <h2 class="title is-size-6 has-text-weight-bold">Generate Reports</h2>
                <div id="generate-bottling-report">
                    <h3 class="subtitle is-size-6">Bottling Report</h3>
                </div>
                <div id="generate-ttb-reports">
                    <h3 class="subtitle is-size-6">TTB Report</h3>
                </div>
            </div>
        </div>

    </div>
</div>