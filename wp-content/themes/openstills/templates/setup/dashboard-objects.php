<div class="box">
    <div class="section-header columns">
        <div class="column is-8">
            <h3 class="title is-size-5">Objects</h3>
            <p class="subtitle is-size-7 has-text-grey">Below, find all defined objects organized by name space.</p>
        </div>
        <div class="column is-4">
            <a class="button is-primary is-pulled-right" href="/settings/object/add/">
                <span class="icon">
                    <i class="fa fa-plus"></i> 
                </span>
                <span>New Object</span>
            </a>
        </div>
    </div>
    <div id="objects-list">
        <?php $objects = getDefintedObjectsByNamespace(); ?>
        <?php foreach ($objects as $namespace => $objectList): ?>
        <h3 class="menu-label"><?php echo $namespace; ?></h3>
        <ul class="menu-list">
            <?php foreach ($objectList as $key => $objectName): ?>
            <li class="is-grey-hover">
                <?php echo $objectName; ?>
                <div class="object-actions is-pulled-right">
                    <a href="/settings/object/edit/<?php echo $namespace . "/" . $objectName; ?>"><i class="fa fa-pencil"></i></a>
                    <a href="/<?php echo $namespace . "/" . $objectName; ?>"><i class="fa fa-eye"></i></a>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endforeach; ?>
    </div>
</div>

<div class="box">
    <div class="section-header columns">
        <div class="column is-8">
            <h3 class="title is-size-5">Namespaces</h3>
            <p class="subtitle is-size-7 has-text-grey">Namespaces are used to better organize your objects and any corresponding code. For instance, within OpenStills are the namespaces <strong>production, tasks, and messages</strong>. This allows us to easily separate functionality for these areas in to their own folders and namespaces.</p>
        </div>
    </div>
    
    <div id="add-namespace-wrapper" class="columns">
        <div class="column is-12">
            <div class="field has-addons">
                <div class="control is-fullwidth">
                    <input class="input" type="text" placeholder="Name your namespace">
                </div>
                <div class="control">
                    <a class="button is-primary" id="save-namespace">Create New Namepsace</a>
                </div>
            </div>
        </div>
    </div>

    <div class="columns">
        <div id="namespaces-list" class="column is-12">
            <?php
            $namespaces = getDefinedNamespaces();
            echo "<ul>";
            if ($namespaces):
                foreach ($namespaces as $namespace):
                    echo '<li>' . $namespace . '</li>';
                endforeach;
            endif;
            echo "</ul>";
            ?>
        </div>
    </div>

</div>