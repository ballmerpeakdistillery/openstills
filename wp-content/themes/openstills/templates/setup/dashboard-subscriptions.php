<div class="box">
    <div class="section-header">
        <h3 class="title is-size-5">My Subscriptions</h3>
    </div>
    <div id="subscriptions-list">
        <?php 
        $userSubscriptions = get_usermeta(get_current_user_id(), "subscription"); 
        if (gettype($userSubscriptions) == "string") {
            $userSubscriptions = array(
                $userSubscriptions
            );
        }
        ?>
        <?php $subscriptions = getSubscriptionList(); ?>
        <?php foreach ($subscriptions as $namespace => $subscriptionOptions): ?>
            <h3 class="menu-label is-size-5"><?php echo $namespace; ?></h3>
            <ul class="menu-list">
                <?php foreach ($subscriptionOptions as $option): ?>
                <li>
                    <span class="is-size-6 has-text-weight-bold"><?php echo $option["label"]; ?></span>
                    <ul class="item-list">
                    <?php foreach ($option["options"] as $itemLabel => $itemValue): ?>
                        <?php if (in_array($itemValue, $userSubscriptions)): ?>
                        <li class="subscription-item added" data-action="notification/removesubscription" data-item="<?php echo $itemValue; ?>" data-callback="addedSubscriptionCallback"><?php echo $itemLabel; ?></li>
                        <?php else: ?>
                        <li class="subscription-item" data-action="notification/addsubscription" data-item="<?php echo $itemValue; ?>" data-callback="addedSubscriptionCallback"><?php echo $itemLabel; ?></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </ul>
                </li>
                <?php endforeach; ?>
            </ul>
        <?php endforeach; ?>


        <?php /*
        <?php $objects = getDefintedObjectsByNamespace(); ?>
        <?php foreach ($objects as $namespace => $objectList): ?>
        <h3 class="menu-label is-size-5"><?php echo $namespace; ?></h3>
        <ul class="menu-list">
            <?php foreach ($objectList as $key => $objectName): ?>
            <li>
                <span class="is-size-6 has-text-weight-bold"><?php echo $objectName; ?></span>
                <ul>
                    <li class="subscription-item" data-action="notification/addsubscription" data-item="<?php echo $objectName . "/create"; ?>" data-callback="addedSubscriptionCallback">Create</li>
                    <li class="subscription-item" data-action="notification/addsubscription" data-item="<?php echo $objectName . "/update"; ?>" data-callback="addedSubscriptionCallback">Update</li>
                    <li class="subscription-item" data-action="notification/addsubscription" data-item="<?php echo $objectName . "/statuschange"; ?>" data-callback="addedSubscriptionCallback">Status Change</li>
                </ul>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endforeach; ?>
        */ ?>
    </div>
</div>