

<div id="menu-setup" class="columns">

    <div class="column is-5">
        <div class="box">
            <h3 class="title is-size-6">Available Items <span class="button is-primary is-pulled-right" data-action="addMenuGroup">Add Group</span></h3>
            <div id="menu-objects-list">
                <p class="menu-label is-size-6">Objects</p>
                <ul>
                <?php $objects = getDefintedObjectsByNamespace(); ?>
                <?php foreach ($objects as $namespace => $objectList): ?>
                <h3 class="menu-label has-text-weight-bold"><?php echo $namespace; ?></h3>
                <ul class="menu-list">
                    <?php foreach ($objectList as $key => $objectName): ?>
                    <li class="is-grey-hover">
                        <?php $objectConfig = getObjectConfig($namespace, $objectName); ?>
                        <?php echo $objectName; ?>
                        <i class="fa fa-plus is-pulled-right action" data-action="addMenuObject" data-namespace="<?php echo $namespace; ?>" data-object="<?php echo $objectName; ?>" data-rewrite="<?php echo $objectConfig["config"]["rewrite"]["slug"]; ?>"></i>
                    </li>
                    <?php endforeach; ?>
                </ul>
                <?php endforeach; ?>
                </ul>
            </div>
            <div id="menu-custom-link">
                <p class="menu-label is-size-6">Custom Link</p>
                <div class="field has-addons">
                    <div class="control is-fullwidth">
                        <input class="input" type="text" placeholder="Custom Link">
                    </div>
                    <div class="control">
                        <a class="button is-primary" id="add-link"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="column is-7">
        <div class="box">
            <h3 class="title is-size-6">My Menu<span class="button is-primary is-pulled-right" data-action="saveMenu">Save Menu</span></h3>
            <div id="menu-items">
                <?php
                $currentUser = wp_get_current_user();
                $userMenu = get_option("osMenu_" . $currentUser->user_login);
                if ($userMenu):
                    foreach ($userMenu as $key => $group):
                    ?>
                    <div class="menu-group">
                        <input type="text" class="input is-small group-name" placeholder="New Group" value="<?php echo $group["label"]; ?>" />
                        <?php foreach ($group["items"] as $key => $groupData): ?>
                            <?php
                            switch ($groupData["type"]) {
                                case "object":
                                    ?>
                                    <div class="menu-item columns" data-namespace="<?php echo $groupData["namespace"]; ?>" data-object="<?php echo $groupData["object"]; ?>" data-type="object" data-rewrite="<?php echo $groupData["rewrite"]; ?>">
                                        <div class="column is-6"><?php echo $groupData["object"]; ?></div>
                                        <div class="column is 5">
                                            <?php if (in_array("add", $groupData["links"])): ?>
                                            <i class="fa fa-plus action enabled" data-action="toggleObjectLink" data-item="add"></i>
                                            <?php else: ?>
                                            <i class="fa fa-plus action" data-action="toggleObjectLink" data-item="add"></i>
                                            <?php endif; ?>
                                            <?php if (in_array("archive", $groupData["links"])): ?>
                                            <i class="fa fa-list action enabled" data-action="toggleObjectLink" data-item="archive"></i>
                                            <?php else: ?>
                                            <i class="fa fa-list action" data-action="toggleObjectLink" data-item="archive"></i>
                                            <?php endif; ?>
                                            <?php if (in_array("settings", $groupData["links"])): ?>
                                            <i class="fa fa-gears action enabled" data-action="toggleObjectLink" data-item="settings"></i>
                                            <?php else: ?>
                                            <i class="fa fa-gears action" data-action="toggleObjectLink" data-item="settings"></i>
                                            <?php endif; ?>
                                        </div>
                                        <div class="column is-1">
                                            <i class="fa fa-trash action" data-action="deleteMenuItem"></i>
                                        </div>
                                    </div>
                                    <?php
                                    break;
                                case "link":

                                    break;
                            }
                            ?>
                        <?php endforeach; ?>
                    </div>
                    <?php
                    endforeach;
                endif
                ?>

            </div>
        </div>
    </div>

</div>

