<h1 class="title">Tasks</h1>

<div id="task-navigation">
    <a class="button is-primary is-active">
        My Tasks
        <span class="count"><?php echo getTaskCount(); ?></span>
    </a>
    <a class="button is-primary">
        <span>All Tasks</span>
        <span class="icon">
            <i class="fa fa-list"></i>
        </span>
    </a>
    <div class="is-pulled-right columns" id="task-actions">
        <div class="column is-8">
            <input type="text" class="input" id="search-posts-input" placeholder="Search Tasks" />
        </div>
        <div class="column is-4">
            <a class="button is-primary" href="/add/base/task">
                <span>New Task</span>
                <span class="icon">
                    <i class="fa fa-plus"></i>
                </span>
            </a>
        </div>
    </div>
</div>

<div id="task-header">
    <div class="columns">
        <div class="column is-2"><span class="subtitle is-size-7 has-text-weight-bold is-uppercase">Status</span></div>
        <div class="column is-5"><span class="subtitle is-size-7 has-text-weight-bold is-uppercase">Title</span></div>
        <div class="column is-2"><span class="subtitle is-size-7 has-text-weight-bold is-uppercase">Priority</span></div>
        <div class="column is-2"><span class="subtitle is-size-7 has-text-weight-bold is-uppercase">Category</span></div>
    </div>
</div>
<div id="task-content">

    <?php
    $myTasks = getMyTasks();
    $statusClasses = array(
        "new"       => "is-primary",
        "in-progress"   => "is-warning",
        "blocked"       => "is-danger",
        "complete"      => "is-light"
    );
    foreach ($myTasks as $task):
        ?>
        <div class="task task-list-item columns is-multiline">
            <div class="status column is-2">
                <?php
                $status = get_post_meta($task->ID, "status", true); ?>
                <a class="button is-small <?php echo $statusClasses[$status]; ?>"><?php echo $status; ?></a>
            </div>
            <div class="title column is-5">
                <a href="<?php echo get_permalink($task->ID); ?>" class="is-size-6 has-text-dark"><?php echo $task->post_title; ?></a>
            </div>
            <div class="priority column is-2">
                <?php echo get_post_meta($task->ID, "priority", true); ?>
            </div>
            <div class="category column is-2">
                <?php
                $terms = wp_get_post_terms($task->ID);
                foreach ($terms as $term):
                    echo '<h4>' . $term . '</h4>';
                endforeach;
                ?>
            </div>
            <div class="actions">

            </div>
        </div>
        <?php
    endforeach;
    ?>

</div>